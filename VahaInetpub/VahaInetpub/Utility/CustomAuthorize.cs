﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using VahaInetpub.Models;

namespace VahaInetpub.Utility
{
    public class CustomAuthorize : AuthorizeAttribute
    {
        protected override bool AuthorizeCore(HttpContextBase httpContext)
        {
            int pwdSessionItem=-1;
            var UserSessionItem = httpContext.Session["UserSessionData"];
            if (httpContext.Session["MustChangePwd"]!=null)
            {
                pwdSessionItem = int.Parse(httpContext.Session["MustChangePwd"].ToString());
            }
            else
            {
                pwdSessionItem = -1;

            }
            

            if (UserSessionItem == null)
            {
                return false;
            }

            if (pwdSessionItem==1)
            {
               
               
                return false;
            }
          
            else
            {
                UserSessionData userInfo = (UserSessionData)httpContext.Session["UserSessionData"];
                

                if (userInfo == null)
                {
                    return false;
                }
               

                HttpCookie AuthTokenCookie = httpContext.Request.Cookies["SunAuthToken"];
                String AuthToken = userInfo.AuthToken;

                if (AuthToken.Equals(""))
                    return false;

                if (AuthTokenCookie == null)
                    return false;

                if (!AuthTokenCookie.Value.Equals(AuthToken))
                    return false;

                AuthorizePageForUser();

            }

            return true;
        }

        protected override void HandleUnauthorizedRequest(AuthorizationContext filterContext)
        {

            int MustChangePwd = -1;
            if (filterContext.HttpContext.Session["MustChangePwd"] != null)
            {
                MustChangePwd = int.Parse(filterContext.HttpContext.Session["MustChangePwd"].ToString());
            }
            else
            {
                MustChangePwd = -1;

            }
            if (MustChangePwd == -1)
            {
                filterContext.Result = new RedirectResult("/login");
            }
            else if (MustChangePwd==1)
            {
                filterContext.Result = new RedirectResult("/mustchangepwd");
            }
        }

        public static void AuthorizePageForUser()
        {

        }
    }

    [AttributeUsage(AttributeTargets.Class | AttributeTargets.Method)]
    public sealed class NoCacheAttribute : ActionFilterAttribute
    {
        public override void OnResultExecuting(ResultExecutingContext filterContext)
        {
            filterContext.HttpContext.Response.Cache.SetExpires(DateTime.UtcNow.AddDays(-1));
            filterContext.HttpContext.Response.Cache.SetValidUntilExpires(false);
            filterContext.HttpContext.Response.Cache.SetRevalidation(HttpCacheRevalidation.AllCaches);
            filterContext.HttpContext.Response.Cache.SetCacheability(HttpCacheability.NoCache);
            filterContext.HttpContext.Response.Cache.SetNoStore();

            base.OnResultExecuting(filterContext);
        }
    }
}