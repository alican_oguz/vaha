﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace VahaInetpub.Utility
{
    public class SessionParamsUtil
    {
        public static String PARAM_TABLE_1 = "PARAMETER_NAMES";
        public static String PARAM_TABLE_2 = "PARAMETER_NAMES_2";

        public static void ClearParameterTable()
        {
            List<String> parameterNames = HttpContext.Current.Session["PARAMETER_NAMES"] != null ? (List<String>)HttpContext.Current.Session["PARAMETER_NAMES"] : null;
            if (parameterNames == null)
                return;

            foreach (var pName in parameterNames)
                HttpContext.Current.Session[pName] = null;

            HttpContext.Current.Session["PARAMETER_NAMES"] = null;
        }

        public static void ClearParameterTable(String paramTableName)
        {
            List<String> parameterNames = HttpContext.Current.Session[paramTableName] != null ? (List<String>)HttpContext.Current.Session[paramTableName] : null;
            if (parameterNames == null)
                return;

            foreach (var pName in parameterNames)
                HttpContext.Current.Session[pName] = null;

            HttpContext.Current.Session[paramTableName] = null;
        }

        public static String addToParameterTable(String param)
        {
            Random random = new Random();
            Int64 l_pName = random.Next(1, 1000000);
            String tmp_pName = l_pName.ToString();

            List<String> parameterNames = HttpContext.Current.Session["PARAMETER_NAMES"] != null ? (List<String>)HttpContext.Current.Session["PARAMETER_NAMES"] : null;
            if (parameterNames != null)
            {
                bool pNameLoop = true;
                while (pNameLoop)
                {
                    bool pNameFound = false;
                    foreach (var pName in parameterNames)
                    {
                        if (tmp_pName.Equals(pName))
                        {
                            l_pName = random.Next(1, 1000000);
                            tmp_pName = l_pName.ToString();
                            pNameFound = true;
                            break;
                        }
                    }
                    if (!pNameFound)
                        pNameLoop = false;
                }
            }
            else
                parameterNames = new List<string>();

            parameterNames.Add(tmp_pName);
            HttpContext.Current.Session["PARAMETER_NAMES"] = parameterNames;
            HttpContext.Current.Session[tmp_pName] = param;

            return tmp_pName;
        }

        public static String addToParameterTable(String paramTableName, String param)
        {
            Random random = new Random();
            Int64 l_pName = random.Next(1, 1000000);
            String tmp_pName = l_pName.ToString();

            List<String> parameterNames = HttpContext.Current.Session[paramTableName] != null ? (List<String>)HttpContext.Current.Session[paramTableName] : null;
            if (parameterNames != null)
            {
                bool pNameLoop = true;
                while (pNameLoop)
                {
                    bool pNameFound = false;
                    foreach (var pName in parameterNames)
                    {
                        if (tmp_pName.Equals(pName))
                        {
                            l_pName = random.Next(1, 1000000);
                            tmp_pName = l_pName.ToString();
                            pNameFound = true;
                            break;
                        }
                    }
                    if (!pNameFound)
                        pNameLoop = false;
                }
            }
            else
                parameterNames = new List<string>();

            parameterNames.Add(tmp_pName);
            HttpContext.Current.Session[paramTableName] = parameterNames;
            HttpContext.Current.Session[tmp_pName] = param;

            return tmp_pName;
        }

        public static Dictionary<String, String> parseParameterString(String parameter)
        {
            Dictionary<String, String> pDict = new Dictionary<string, string>();

            string[] words = parameter.Split(new string[] { ";;" }, StringSplitOptions.RemoveEmptyEntries);

            if (words == null)
                return pDict;

            foreach (string word in words)
            {
                string[] values = word.Split(new char[] { ':' }, StringSplitOptions.RemoveEmptyEntries);
                if (values != null)
                {
                    if (!pDict.ContainsKey(values[0]))
                    {
                        if (values.Length == 2)
                            pDict.Add(values[0], values[1]);
                        else
                            pDict.Add(values[0], "");
                    }
                }
            }

            return pDict;
        }

        public static String createParameter(String pName, String pValue)
        {
            String ret = pName + ":" + pValue + ";;";
            return ret;
        }

        public static void ClearBreadCrumbParams()
        {
            HttpContext.Current.Session.Remove("BC_PAGE_1_NAME");
            HttpContext.Current.Session.Remove("BC_PAGE_1_LINK");
            HttpContext.Current.Session.Remove("BC_PAGE_2_NAME");
            HttpContext.Current.Session.Remove("BC_PAGE_2_LINK");
            HttpContext.Current.Session.Remove("BC_PAGE_3_NAME");
            HttpContext.Current.Session.Remove("BC_PAGE_3_LINK");
            HttpContext.Current.Session.Remove("BC_PAGE_4_NAME");
            HttpContext.Current.Session.Remove("BC_PAGE_4_LINK");
        }

        public static Boolean HasValidSession()
        {
            return HttpContext.Current.Session["UserSessionData"] != null;
        }

        public static Boolean ValidateSessionParam(String refcode, out GenericJsonError response, out Dictionary<string, string> parameters)
        {
            String lang = CommonConfiguration.SiteLanguage();
            response = new GenericJsonError(true, "", "", "");
            parameters = new Dictionary<string, string>();

            if (refcode == null || refcode.Equals(""))
            {
                response = new GenericJsonError(false, "", "", "ValidateSessionParam:CM:Error:1");
                return false;
            }


            String strParameter = "";
            if (HttpContext.Current.Session["PARAMETER_NAMES"] != null)
            {
                List<String> parameterList = (List<String>)HttpContext.Current.Session["PARAMETER_NAMES"];
                if (parameterList == null)
                {
                    response = new GenericJsonError(false, "", "", "ValidateSessionParam:CM:Err:2");
                    return false;
                }

                foreach (String tmpParameter in parameterList)
                    if (tmpParameter.Equals(refcode))
                        strParameter = (String)HttpContext.Current.Session[tmpParameter];
            }
            else
            {
                response = new GenericJsonError(false, "", "", "ValidateSessionParam:CM:Err:3");
                return false;
            }

            if (strParameter.Equals(""))
            {
                response = new GenericJsonError(false, "", "", "ValidateSessionParam:CM:Err:4");
                return false;
            }

            try
            {
                parameters = SessionParamsUtil.parseParameterString(strParameter);
                if (parameters == null)
                {
                    response = new GenericJsonError(false, "", "", "ValidateSessionParam:CM:Err:5");
                    return false;
                }

            }
            catch (Exception ex)
            {
                response = new GenericJsonError(false, "", ex.Message, "ValidateSessionParam:CM:Err:6");
                return false;
            }

            return true;
        }

    }
}