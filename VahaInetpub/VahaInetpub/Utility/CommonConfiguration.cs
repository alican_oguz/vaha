﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Configuration;
using VahaInetpub.Models;

namespace VahaInetpub.Utility
{
    public class CommonConfiguration
    {
        public static string ERR_CODE_FORM_DATA_NOT_AVAILABLE = "-90";
        public static string ERR_CODE_OPERATION_FAILED_NO_ERR_DESC = "-91";
        public static string ERR_CODE_OPERATION_EXCEPTION = "-92";
        public static string ERR_CODE_OPERATION_FAILED = "-93";

        public static string SiteLanguage()
        {
            if (HttpContext.Current != null && HttpContext.Current.Session != null)
            {
                String lang = HttpContext.Current.Session["siteLanguage"] != null ? (String)HttpContext.Current.Session["siteLanguage"] : "tr-TR";
                HttpContext.Current.Session["siteLanguage"] = lang;
                return lang;
            }
            else
            {
                return "tr-TR";
            }

           
        }

        public static string TopMenuLogo(int type)
        {
            String lang = (String)HttpContext.Current.Session["siteLanguage"];
            String txtMenuLogo = "";

            if (type == 1)
            {
                txtMenuLogo = StringResources.getString(StringResources.RES_LABEL, "TOP_MEU_LOGO_TXT", lang);
            }
            else
            {
                txtMenuLogo = StringResources.getString(StringResources.RES_LABEL, "TOP_MEU_LOGO_TXT_SHORT", lang);
            }

            return txtMenuLogo;
        }

        public static string UserDefaultProfileImg()
        {
            String lang = (String)HttpContext.Current.Session["siteLanguage"];
            String imgPath = StringResources.getString(StringResources.RES_LABEL, "DEFAULT_PROFILE_IMG_PATH", lang);
            return imgPath;
        }

        public static string SessionUserName()
        {
            if (HttpContext.Current != null && HttpContext.Current.Session != null)
            {
                 UserSessionData userInfoItem = (UserSessionData)HttpContext.Current.Session["UserSessionData"];

            if (userInfoItem == null)
            {
                return "";
            }

            return userInfoItem.UserEmail;
            }
            else
            {
                return "NoSessionUser";
            }
        }
    }
}