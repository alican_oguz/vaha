﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace VahaInetpub.Utility
{
    public class FormDataNameValue
    {
        public String name { get; set; }
        public String value { get; set; }
    }

    public class FormDataRetreiver
    {
        public static void getFormData(List<FormDataNameValue> formdatapair, out Dictionary<String, String> valuesDict)
        {
            valuesDict = new Dictionary<string, string>();

            foreach (FormDataNameValue nvp in formdatapair)
            {
                String dtName = nvp.name;
                String dtVal = nvp.value;
                if (!valuesDict.ContainsKey(dtName))
                    valuesDict[dtName] = dtVal;
            }

            return;
        }

        public static void getFormData(List<FormDataNameValue> formdatapair, out Dictionary<String, object> valuesDict)
        {
            valuesDict = new Dictionary<string, object>();

            foreach (FormDataNameValue nvp in formdatapair)
            {
                String dtName = nvp.name;
                String dtVal = nvp.value;
                if (!valuesDict.ContainsKey(dtName))
                    valuesDict[dtName] = dtVal;
                else
                {
                    object tmpObj = valuesDict[dtName];
                    if (tmpObj.GetType() == typeof(string))
                    {
                        String tmpVal = (String)valuesDict[dtName];
                        List<string> tmpList = new List<string>();
                        tmpList.Add(tmpVal);
                        tmpList.Add(dtVal);
                        valuesDict.Remove(dtName);
                        valuesDict[dtName] = tmpList;
                    }
                    else if (tmpObj.GetType() == typeof(List<string>))
                    {
                        List<string> tmpList = (List<string>)valuesDict[dtName];
                        tmpList.Add(dtVal);
                        valuesDict.Remove(dtName);
                        valuesDict[dtName] = tmpList;
                    }
                }
            }

            return;
        }
    }

    public class ErrorModalData
    {
        public String ModalId { get; set; }
        public String Icon { get; set; }
        public String Title { get; set; }
        public String Body { get; set; }
        public String Btn1Text { get; set; }
        public String Btn1Class { get; set; }
        public String Btn1Href { get; set; }
        public String Btn1Loading { get; set; }
        public String Btn2Text { get; set; }
        public String Btn2Class { get; set; }
        public String Btn2Href { get; set; }
        public String Btn2Loading { get; set; }

        public ErrorModalData(String pModalId, String pTitle, String pIcon, String pBody, String pBtn1Text, String pBtn1Class, String pBtn1Href, String pBtn2Text, String pBtn2Class, String pBtn2Href)
        {
            ModalId = pModalId;
            Icon = pIcon;
            Title = pTitle;
            Body = pBody;
            Btn1Class = pBtn1Class;
            Btn1Href = pBtn1Href;
            Btn1Text = pBtn1Text;
            Btn2Class = pBtn2Class;
            Btn2Href = pBtn2Href;
            Btn2Text = pBtn2Text;
        }
    }

    public class GenericJsonError
    {
        public Boolean result { get; set; }
        public String resultmsg { get; set; }
        public String errcode { get; set; }
        public String errmsg { get; set; }
        public String description { get; set; }
        public String data1 { get; set; }
        public String data2 { get; set; }
        public String data3 { get; set; }
        public String data4 { get; set; }
        public String data5 { get; set; }

        public GenericJsonError()
        {
            result = false;
            resultmsg = "N/A";
            errcode = "-99";
            errmsg = "N/A";
            description = "";
            data1 = "";
            data2 = "";
            data3 = "";
            data4 = "";
            data5 = "";
        }

        public GenericJsonError(Boolean pResult, String pResultmsg, String pErrcode, String pErrmsg)
        {
            result = pResult;
            resultmsg = pResultmsg;
            errcode = pErrcode;
            errmsg = pErrmsg;
            description = "";
            data1 = "";
            data2 = "";
            data3 = "";
            data4 = "";
            data5 = "";
        }
    }

    public class UnexpectedErrorModal
    {
        public static GenericJsonError GenerateModal(String lang, String errDesc, String modalId)
        {
            GenericJsonError jResp = new GenericJsonError(false, "", "", "");

            String ModalTitle = StringResources.getString(StringResources.RES_ERR, "DEFAULT_ERR_MODAL_TITLE", lang);
            String ModalError = StringResources.getString(StringResources.RES_ERR, "DEFAULT_ERR_MODAL_MSG", lang);
            String ModalClose = StringResources.getString(StringResources.RES_LABEL, "LBL_CLOSE", lang);

            ErrorModalData errModal = new ErrorModalData(
                modalId, ModalTitle, "F",
                ModalError + "<br><br><strong>{" + errDesc + "}</strong>",
                ModalClose, "default", "dismiss",
                "", "", ""
            );

            HttpContext.Current.Session["ModalData"] = errModal;

            return jResp;
        }
    }

    public class EditFormDataItem
    {
        public static String ItemTypeText = "text";
        public static String ItemTypeHidden = "hidden";
        public static String ItemTypePwd = "password";
        public static String ItemTypeTextArea = "textarea";
        public static String ItemTypeImageSelect= "imgselect";

        public String itemid { get; set; }
        public String itemtype { get; set; }
        public String itemval { get; set; }
        public String imgwidth { get; set; }
        public String imgheight { get; set; }
        public Boolean disabled { get; set; }
        public List<EditFormSelectItem> selectitems { get; set; }

        public EditFormDataItem()
        {
            this.itemid = "";
            this.itemtype = "";
            this.itemval = "";
            this.imgwidth = "";
            this.imgheight = "";
            this.disabled = false;
            this.selectitems = new List<EditFormSelectItem>();
        }

        public EditFormDataItem(String pItemid, String pItemtype, String pItemval, Boolean pDisabled)
        {
            this.itemid = pItemid;
            this.itemtype = pItemtype;
            this.itemval = pItemval;
            this.disabled = pDisabled;
            this.imgwidth = "";
            this.imgheight = "";
            this.selectitems = new List<EditFormSelectItem>();
        }

        public EditFormDataItem(String pItemid, String pItemtype, String pItemval, List<EditFormSelectItem> pSelectItems, Boolean pDisabled)
        {
            this.itemid = pItemid;
            this.itemtype = pItemtype;
            this.itemval = pItemval;
            this.disabled = pDisabled;
            this.selectitems = pSelectItems;
            this.imgwidth = "";
            this.imgheight = "";
        }

        public EditFormDataItem(String pItemid, String pItemtype, String pItemval, String pImgWidth, String pImgHeight, Boolean pDisabled)
        {
            this.itemid = pItemid;
            this.itemtype = pItemtype;
            this.itemval = pItemval;
            this.disabled = pDisabled;
            this.selectitems = new List<EditFormSelectItem>(); ;
            this.imgwidth = pImgWidth;
            this.imgheight = pImgHeight;
        }

        public void AddSelectItem(String pSitemval, String pSitemdisp)
        {
            EditFormSelectItem sItem = new EditFormSelectItem();
            sItem.sitemval = pSitemval;
            sItem.sitemdisp = pSitemdisp;
            this.selectitems.Add(sItem);
        }

        public static List<EditFormSelectItem> GenerateListFromKeyValue(List<KeyValuePair<String, String>> pKvList)
        {
            List<EditFormSelectItem> rList = new List<EditFormSelectItem>();
            foreach(KeyValuePair<String,String> kvItem in pKvList)
            {
                EditFormSelectItem sItem = new EditFormSelectItem(kvItem.Key, kvItem.Value);
                rList.Add(sItem);
            }

            return rList;
        }

        public static List<EditFormSelectItem> GenerateListFromDictionary(Dictionary<String, String> pDict)
        {
            List<EditFormSelectItem> rList = new List<EditFormSelectItem>();
            foreach (String itemKey in pDict.Keys)
            {
                EditFormSelectItem sItem = new EditFormSelectItem(itemKey, pDict[itemKey]);
                rList.Add(sItem);
            }

            return rList;
        }
    }

    public class EditFormSelectItem
    {
        public String sitemval { get; set; }
        public String sitemdisp { get; set; }

        public EditFormSelectItem()
        {
            this.sitemval = "";
            this.sitemdisp = "";
        }

        public EditFormSelectItem(String pItemVal, String pItemDisp)
        {
            this.sitemval = pItemVal;
            this.sitemdisp = pItemDisp;
        }
    }

}