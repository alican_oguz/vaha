function showChangePassword() {
    $('#changepwd').find('.modal-content').load('/Utility/ShowChangePassword', function () {
        $('#changepwd').modal({
            backdrop: 'static',
            keyboard: false
        })
    });
}

function showInfoModal(modalId) {
    $('#' + modalId).find('.modal-content').load('/Utility/ShowInfoModal', function () {
        $('#' + modalId).modal({
            backdrop: 'static',
            keyboard: false
        })
    });
}

function showDefaultErrorModal(modalId, errcode, errdesc) {
    $('#' + modalId).find('.modal-content').load('/Utility/ShowDefaultErrorModal?errcode=' + errcode + '&errdesc=' + errdesc, function () {
        $('#' + modalId).modal({
            backdrop: 'static',
            keyboard: false
        })
    });
}

function showModalBodyMessage(modalId) {
    $('#' + modalId).find('.modal-body').load('/Utility/ShowInfoModalBodyMessage', function () {
    });
}

function InfoModalBeforeSend() {
    $('#ModalBtn1').button('loading');
    $('#ModalBtn2').prop('disabled', true);
    $('.modal-title-loading').show();
}

function InfoMoadResetError() {
    $('#ModalBtn1').button('reset');
    $('#ModalBtn1').prop('disabled', true);
    $('#ModalBtn2').prop('disabled', false);
    $('.modal-footer-inf').addClass('err-msg');
    $('.modal-title-loading').hide();
}

function InfoMoadResetDisplayMsg() {
    $('.modal-title-loading').hide();
    $('#ModalBtn1').button('reset');
    $('#ModalBtn1').hide();
    $('#ModalBtn2').prop('disabled', false);
    $('#ModalBtn2').text(defaultCloseLabel);
    $('#ModalBtn2').unbind('click');
    $('#ModalBtn2').click(function () { window.location.href = window.location.href });
}

function confirmLogout() {
    $.ajax({
        type: 'GET',
        url: '/Login/ConfirmLogout',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            var jresult = data;
            if (jresult.hasOwnProperty('result') && jresult.result == true) {
                showInfoModal('infoModal');
            }
            else if (jresult.hasOwnProperty('result') && jresult.result == false) {
                showDefaultErrorModal('infoModal', '1', 'Logout;Confirm;Success;1');
            }
            else {
                showDefaultErrorModal('infoModal', '1', 'Logout;Confirm;Success;2');
            }
        },
        error: function (xhr, ajaxOptions, thrownError) {
            console.log(xhr);
            showDefaultErrorModal('infoModal', '1', 'Logout;Confirm;Error;1');
        }
    });
}

function doLogout() {
    $.ajax({
        type: 'GET',
        url: '/Login/DoLogout',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        beforeSend: function () {
            InfoModalBeforeSend();
        },
        success: function (data) {
            var jresult = data;
            if (jresult.hasOwnProperty('result') && jresult.result == true) {
                window.location.href = '/login';
            }
            else if (jresult.hasOwnProperty('result') && jresult.result == false) {
                InfoMoadResetError();
                $('.modal-footer-inf').html(jresult.errmsg);
            }
            else {
                InfoMoadResetError();
                $('.modal-footer-inf').html(defaultErrMsg.replace("%err%", "Logout;DoLogout;Success;1"));
            }
        },
        error: function (xhr, ajaxOptions, thrownError) {
            console.log(xhr);
            InfoMoadResetError();
            $('.modal-footer-inf').html(defaultErrMsg.replace("%err%", "Logout;DoLogout;Error;1"));
        }
    });
}

function DoConfirmAndAction(header, msg, url) {
    $.ajax({
        type: 'GET',
        url: '/Login/HasValidSession',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            var jresult = data;
            if (jresult.hasOwnProperty('result') && jresult.result == true) {
                var myObj = {
                    'actionurl': url,
                    'confirmmsg': msg,
                    'confirmtitle': header
                };
                
                $.ajax({
                    type: 'POST',
                    url: JsAppRoot + 'Utility/ConfirmAction',
                    data: JSON.stringify(myObj),
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (data) {
                        var jresult = data;
                        if (jresult.hasOwnProperty('result') && jresult.result == true) {
                            showInfoModal('infoModal');
                        }
                        else if (jresult.hasOwnProperty('result') && jresult.result == false) {
                            showInfoModal('infoModal');
                        }
                        else {
                            showDefaultErrorModal('infoModal', '1', 'DoConfirmAndAction;Confirm;Success;1');
                        }
                    },
                    error: function (xhr, textStatus, errorThrown) {
                        console.log(xhr);
                        showDefaultErrorModal('infoModal', '1', 'DoConfirmAndAction;Confirm;Error;1');
                    }
                });
            }
            else if (jresult.hasOwnProperty('result') && jresult.result == false) {
                showInfoModal('infoModal');
            }
        },
        error: function (xhr, textStatus, errorThrown) {
            console.log(xhr);
            showDefaultErrorModal('infoModal', '1', 'DoConfirmAndAction;ValidSession;Error;1');
        }
    });
}

function DoConfirmedAction(submitUrl) {

    $.ajax({
        type: 'GET',
        url: submitUrl,
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        beforeSend: function () {
            InfoModalBeforeSend();
        },
        success: function (data) {
            var jresult = data;
            if (jresult.hasOwnProperty('result') && jresult.result == true) {
                if (jresult.hasOwnProperty('resultmsg') && jresult.resultmsg == 'dispmsg') {
                    showModalBodyMessage('infoModal');
                    InfoMoadResetDisplayMsg();
                }
                else if (jresult.hasOwnProperty('resultmsg') && jresult.resultmsg == 'redirect') {
                    window.location.href = jresult.data1;
                }
                else {
                    window.location.href = window.location.href;
                }
            }
            else if (jresult.hasOwnProperty('result') && jresult.result == false) {
                if (jresult.hasOwnProperty('errcode') && jresult.errcode == 'timeout') {
                    setTimeout(function () { window.location.href = '/login' }, 500);
                }
                else if (jresult.hasOwnProperty('errcode')) {
                    InfoMoadResetError();
                    $('.modal-footer-inf').html(jresult.errmsg);
                }
                else {
                    InfoMoadResetError();
                    $('.modal-footer-inf').html(defaultErrMsg.replace("%err%", "DoConfirmedAction;Do;Success;1"));
                }
            }
            else {
                InfoMoadResetError();
                $('.modal-footer-inf').html(defaultErrMsg.replace("%err%", "DoConfirmedAction;Do;Success;2"));
            }
        },
        error: function (e) {
            InfoMoadResetError();
            $('.modal-footer-inf').html(defaultErrMsg.replace("%err%", "DoConfirmedAction;Do;Error;2"));
        }
    });
}

function EditFormLoadData(purl, refcode, formid) {

    EditFormLoading();
    $.ajax({
        type: 'GET',
        url: '/Login/HasValidSession',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            var jresult = data;
            if (jresult.hasOwnProperty('result') && jresult.result == true) {
                var myObj = {
                    'refcode': refcode
                };
                $.ajax({
                    type: 'POST',
                    url: JsAppRoot + purl,
                    data: JSON.stringify(myObj),
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (data) {
                        var jresult = data;
                        if (jresult.hasOwnProperty('result') && jresult.result == true) {
                            EditFormFill(jresult.data1);
                            EditFormRemoveLoading();
                        }
                        else if (jresult.hasOwnProperty('result') && jresult.result == false) {
                            showInfoModal('infoModal');
                        }
                        else {
                            showDefaultErrorModal('infoModal', '1', 'EditFormLoadData;' + purl + ';' + formid + ';1');
                        }
                    },
                    error: function (xhr, textStatus, errorThrown) {
                        console.log(xhr);
                        showDefaultErrorModal('infoModal', '1', 'EditFormLoadData;' + purl + ';' + formid + ';2');
                    }
                });
            }
            else if (jresult.hasOwnProperty('result') && jresult.result == false) {
                showInfoModal('infoModal');
            }
        },
        error: function (xhr, textStatus, errorThrown) {
            console.log(xhr);
            showDefaultErrorModal('infoModal', '1', 'EditFormLoadData;' + purl + ';' + formid + ';3');
        }
    });
}

function EditFormLoading() {
    $('.form-title-loading').show();
    $('.edit-form-btn').button('loading');
}

function EditFormRemoveLoading() {
    $('.form-title-loading').hide();
    $('.edit-form-btn').button('reset');
}

function EditFormFill(data) {
    var fomrdata = jQuery.parseJSON(data);
    $.each(fomrdata, function () {
        if (this.itemtype == 'text') {
            $('#' + this.itemid).val(this.itemval);
            $('#' + this.itemid).prop('disabled', this.disabled);
        }
        else if (this.itemtype == 'p') {
            $('#' + this.itemid).html(this.itemval);
        }
        else if (this.itemtype == 'img') {
            $('#' + this.itemid).val(this.itemval);
            imgholder = $('<div />').attr('id', this.itemid + "_img").addClass('inline-edit-img')
            imgdiv = $('<div />').width(this.imgwidth).height(this.imgheight).css('background-size', '100%').css('background-image', 'url(\'' + this.itemval + '\')').appendTo($(imgholder));
            $(imgholder).width(parseInt(this.imgwidth) + 12);
            $(imgholder).height(parseInt(this.imgheight) + 12);
            $('#' + this.itemid).parent().append($(imgholder));
        }
        else if (this.itemtype == 'textarea') {
            $('#' + this.itemid).val(this.itemval);
            $('#' + this.itemid).prop('disabled', this.disabled);
            $('#' + this.itemid).wysihtml5();
        }
        else if (this.itemtype == 'select') {
            var selectlist = this.selectitems;
            var selectitemid = this.itemid;
            $.each(selectlist, function () {
                $('<option>').val(this.sitemval).text(this.sitemdisp).appendTo($('#' + selectitemid));
            });
            $('#' + this.itemid).val(this.itemval);
            $('#' + this.itemid).prop('disabled', this.disabled);
        }
        else if (this.itemtype == 'imgselect' && this.itemval != '') {
            $('#' + this.itemid).val(this.itemval);
            imgholder = $('<div />').attr('id', this.itemid + "_img").addClass('inline-edit-img')
            imgdiv = $('<div />').width(this.imgwidth).height(this.imgheight).css('background-size', '100%').css('background-image', 'url(\'' + this.itemval + '\')').appendTo($(imgholder));
            $(imgholder).width(parseInt(this.imgwidth) + 12);
            $(imgholder).height(parseInt(this.imgheight) + 12);
            $('#' + this.itemid).parent().append($(imgholder));

            imgaction = $('<div />').addClass('inline-edit-img-action').height(parseInt(this.imgheight) + 12);
            $('<div/>').attr('id', this.itemid + "_img_delete").addClass('imgaction-delete').html(txtDelete).appendTo($(imgaction)).attr('onclick', "deleteCkFinder('" + this.itemid + "')");
            $('<div/>').attr('id', this.itemid + "_img_select").addClass('imgaction-update').html(txtUpdate).appendTo($(imgaction)).attr('onclick', "changeCkFinder('" + this.itemid + "')");
            $('#' + this.itemid).parent().append($(imgaction));


            imgaction = $('<div />').addClass('inline-edit-img-action-noimg');
            $('<div/>').attr('id', this.itemid + "_img_select").addClass('imgaction-select').html(txtSelectImg).appendTo($(imgaction)).attr('onclick', "showCkFinder('" + this.itemid + "')");
            $('#' + this.itemid).parent().append($(imgaction).css("display","none"));

        }
        else if (this.itemtype == 'imgselect' && this.itemval == '') {
            imgholder = $('<div />').attr('id', this.itemid + "_img").addClass('inline-edit-img');
            $(imgholder).width(12);
            $(imgholder).height(12);
            $('#' + this.itemid).parent().append($(imgholder));
            $(imgholder).hide();

            imgaction = $('<div />').addClass('inline-edit-img-action-noimg');
            $('<div/>').attr('id', this.itemid + "_img_select").addClass('imgaction-select').html(txtSelectImg).appendTo($(imgaction));
            $('#' + this.itemid).parent().append($(imgaction));
            $('#' + this.itemid + '_img_select').attr('onclick', "showCkFinder('" + this.itemid + "')");
        }
    });
}

function AnyFormLoading (arrButtons) {
    $.each(arrButtons, function (index, value) {
        $('#' + value).button('loading');
    });
    $('.form-title-loading').show();
}

function AnyFormRemoveLoading(arrButtons) {
    $.each(arrButtons, function (index, value) {
        $('#' + value).button('reset');
    });
    $('.form-title-loading').hide();
}

function imgselectdiv(itemid) {
    imgholder = $('<div />').attr('id', itemid + "_img").addClass('inline-edit-img');
    $(imgholder).width(12);
    $(imgholder).height(12);
    $('#' + itemid).parent().append($(imgholder));
    $(imgholder).hide();

    imgaction = $('<div />').addClass('inline-edit-img-action-noimg');
    $('<div/>').attr('id', itemid + "_img_select").addClass('imgaction-select').html(txtSelectImg).appendTo($(imgaction));
    $('#' + itemid).parent().append($(imgaction));
    $('#' + itemid + '_img_select').attr('onclick', "showCkFinder('" + itemid + "')");
}


function showCkFinder(itemid) {
    var height = "16";
    var width = "16";
    if (itemid.indexOf("Image") > -1) {
        height = "120";
        width = "120";
    }
    if (itemid.indexOf("BgImage") > -1) {
        height = "180";
        width = "320";
    }

    var ckfinder1 = new CKFinder();
    ckfinder1.selectActionFunction = function (fileUrl) {
        $("#" + itemid).val(fileUrl)
        $('#' + itemid + '_img_select').parent().hide();

        imgdiv = $('<div />').width(parseInt(width) + 12).height(parseInt(height) + 12).css('background-size', '100%').css('background-image', 'url(\'' + fileUrl + '\')').css('background-repeat', 'no-repeat').appendTo($(imgholder));
        $('#' + itemid + '_img').append($(imgdiv)).show();
        $('#' + itemid + '_img').width(parseInt(width) + 12);
        $('#' + itemid + '_img').height(parseInt(height) + 12);

        imgaction = $('<div />').addClass('inline-edit-img-action').height(parseInt(height) + 12);
        $('<div/>').attr('id', itemid + "_img_delete").addClass('imgaction-delete').html(txtDelete).appendTo($(imgaction)).attr('onclick', "deleteCkFinder('" + itemid + "')");
        $('<div/>').attr('id', itemid + "_img_select").addClass('imgaction-update').html(txtUpdate).appendTo($(imgaction)).attr('onclick', "changeCkFinder('" + itemid + "')");
        $('#' + itemid).parent().append($(imgaction));

    };
    ckfinder1.popup();
}
function changeCkFinder(itemid) {
    var ckfinder1 = new CKFinder();

    ckfinder1.selectActionFunction = function (fileUrl) {
        $("#" + itemid).val(fileUrl)
        $('#' + itemid + '_img div:first').css('background-image', 'url(\'' + fileUrl + '\')');

    };
    ckfinder1.popup();
}
function deleteCkFinder(itemid) {
    $("#" + itemid).val("");
    $('#' + itemid + '_img_delete').parent().remove();
    $('#' + itemid + '_img_select').parent().show();
    $('#' + itemid + '_img div:first').remove();
    $('#' + itemid + '_img').hide();
}

function ShowNotAuthorizedUser(header,message) {
    $('#processResultHeader').html(header);
    $('#processResultBody').html(message);
    $('#processResultModal').modal('show');
    $('#btnProcessClose').click(function () {
        $('#processResultModal').modal('hide');
    });
    
}