﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using VahaInetpub.Utility;

namespace VahaInetpub.Models
{
    public class ContractPaymentsObject
    {
        public int PaymentId { get; set; }
        public int ContractId { get; set; }
        public decimal Amount { get; set; }
        public DateTime PaymentDate { get; set; }
        public string Status { get; set; }

        private static String lang = CommonConfiguration.SiteLanguage();
        CultureInfo culture = CultureInfo.CreateSpecificCulture(lang);
        VAHAEntities entity = null;
        public ContractPaymentsObject()
        {
            this.PaymentId = 0;
            this.ContractId = 0;
            this.Amount = 0;
            this.PaymentDate = DateTime.MinValue;
            this.Status = "";

        }
        public ContractPaymentsObject(List<FormDataNameValue> formdatapair)
        {
            Dictionary<String, object> valuesDict = new Dictionary<string, object>();
            FormDataRetreiver.getFormData(formdatapair, out valuesDict);

            this.PaymentId = 0;
            this.ContractId = 0;
            this.Amount = 0;
            this.PaymentDate = DateTime.MinValue;
            this.Status = valuesDict.ContainsKey("Status") && valuesDict["Status"] != null ? (string)valuesDict["Status"] : "";
        }

        public String[] Create()
        {
            try
            {
                String[] result = { "true", "", "" };
                entity = new VAHAEntities();
                Vaha_Contract_Payments vahaContractPaymentTempObj = null;
                String requiredErrMsg = StringResources.getString(StringResources.RES_ERR, "GENERIC_REQUIRED_ERR", lang);
                if (this.ContractId.Equals(0))
                {
                    result[0] = "false";
                    result[1] = "-1";
                    result[2] = String.Format(requiredErrMsg, "ContractId");
                }
                else if (this.Amount.Equals(0))
                {
                    result[0] = "false";
                    result[1] = "-1";
                    result[2] = String.Format(requiredErrMsg, "Amount");
                }

                if (result[0].Equals("false"))
                    return result;


                vahaContractPaymentTempObj = new Vaha_Contract_Payments();
                vahaContractPaymentTempObj.ContractId = this.ContractId;
                vahaContractPaymentTempObj.PaymentDate = this.PaymentDate;
                vahaContractPaymentTempObj.Status = this.Status;
                entity.Vaha_Contract_Payments.Add(vahaContractPaymentTempObj);
                entity.SaveChanges();

                result[0] = "true";
                result[1] = "";
                result[2] = "";

                return result;
            }
            catch (Exception ex)
            {
                String[] result = new String[3];
                result[0] = "false";
                result[1] = "-2";
                result[2] = ex.Message;
                return result;
            }
        }

        public List<ContractPaymentsObject> GetBankByContractId(int contactId)
        {
            List<ContractPaymentsObject> tmpObjList = null;
            ContractPaymentsObject tmpObj = null;
            try
            {
                entity = new VAHAEntities();

                List<Vaha_Contract_Payments> contractPaymentObj = entity.Vaha_Contract_Payments.Where(
                    BankSme => BankSme.ContractId == contactId).ToList();

                if (contractPaymentObj != null)
                {
                    foreach (Vaha_Contract_Payments item in contractPaymentObj)
                    {
                        tmpObj = new ContractPaymentsObject();
                        tmpObj.PaymentId = item.PaymentId;
                        tmpObj.ContractId = item.ContractId;
                        tmpObj.PaymentDate = item.PaymentDate.HasValue ? item.PaymentDate.Value : DateTime.MinValue;
                        tmpObj.Status = item.Status;
                        tmpObjList.Add(tmpObj);
                    }
                    return tmpObjList;
                }
                else
                {
                    tmpObjList = null;
                    return tmpObjList;
                }
            }
            catch (Exception ex)
            {
                tmpObjList = null;
                return tmpObjList;
            }
        }
    }
}