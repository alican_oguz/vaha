﻿using System;
using System.Globalization;
using VahaInetpub.Utility;

namespace VahaInetpub.Models
{
    public class AccountObject
    {
        public string Bank { get; set; }
        public string Branch { get; set; }
        public string Number { get; set; }
        public string Balance { get; set; }

        private static String lang = CommonConfiguration.SiteLanguage();
        CultureInfo culture = CultureInfo.CreateSpecificCulture(lang);
        VAHAEntities entity = null;

        public AccountObject()
        {
        }
    }
}