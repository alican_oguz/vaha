﻿using System;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Security.Cryptography;
using System.Globalization;
using System.Web;
using VahaInetpub.Utility;
using System.Net;
using System.Net.Mail;
using System.Web.Configuration;

namespace VahaInetpub.Models
{
    public class BankSmeObject
    {
        public int BankId { get; set; }
        public int SmeId { get; set; }
        public string Status { get; set; }

        private static String lang = CommonConfiguration.SiteLanguage();
        CultureInfo culture = CultureInfo.CreateSpecificCulture(lang);
        VAHAEntities entity = null;
        public BankSmeObject()
        {
            this.BankId = 0;
            this.SmeId = 0;
            this.Status = "";
        }
        public BankSmeObject(List<FormDataNameValue> formdatapair)
        {
            Dictionary<String, object> valuesDict = new Dictionary<string, object>();
            FormDataRetreiver.getFormData(formdatapair, out valuesDict);
            this.BankId = 0;
            this.SmeId = 0;
            this.Status = valuesDict.ContainsKey("Status") && valuesDict["Status"] != null ? (string)valuesDict["Status"] : "";
        }

        public String[] Create()
        {
            try
            {
                String[] result = { "true", "", "" };
                entity = new VAHAEntities();
                Vaha_Bank_Sme vahaBankSmeTempObj = null;
                String requiredErrMsg = StringResources.getString(StringResources.RES_ERR, "GENERIC_REQUIRED_ERR", lang);
                if (this.SmeId.Equals(0))
                {
                    result[0] = "false";
                    result[1] = "-1";
                    result[2] = String.Format(requiredErrMsg, "SmeId");
                }
                else if (this.Status.Equals(""))
                {
                    result[0] = "false";
                    result[1] = "-1";
                    result[2] = String.Format(requiredErrMsg, "Status");
                }

                if (result[0].Equals("false"))
                    return result;


                vahaBankSmeTempObj = new Vaha_Bank_Sme();
                vahaBankSmeTempObj.SmeId = this.SmeId;
                vahaBankSmeTempObj.Status = this.Status;
                entity.Vaha_Bank_Sme.Add(vahaBankSmeTempObj);
                entity.SaveChanges();

                result[0] = "true";
                result[1] = "";
                result[2] = "";

                return result;
            }
            catch (Exception ex)
            {
                String[] result = new String[3];
                result[0] = "false";
                result[1] = "-2";
                result[2] = ex.Message;
                return result;
            }
        }

        public List<BankSmeObject> GetBankBySmeId(int smeId)
        {
            List<BankSmeObject> tmpObjList = null;
            BankSmeObject tmpObj = null;
            try
            {
                entity = new VAHAEntities();

                List<Vaha_Bank_Sme> bankSmeObj = entity.Vaha_Bank_Sme.Where(
                    BankSme => BankSme.SmeId == smeId).ToList();

                if (bankSmeObj != null)
                {
                    foreach (Vaha_Bank_Sme item in bankSmeObj)
                    {
                        tmpObj = new BankSmeObject();
                        tmpObj.BankId = item.BankId;
                        tmpObj.SmeId = item.SmeId;
                        tmpObj.Status = item.Status;
                        tmpObjList.Add(tmpObj);
                    }
                    return tmpObjList;
                }
                else
                {
                    tmpObjList = null;
                    return tmpObjList;
                }
            }
            catch (Exception ex)
            {
                tmpObjList = null;
                return tmpObjList;
            }
        }
    }

}
