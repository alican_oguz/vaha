﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using VahaInetpub.Utility;

namespace VahaInetpub.Models
{
    public class ContractObjects
    {
        public int ContractId  { get; set; }
        public int ContractOwnerId  { get; set; }
        public int ContractSellerId  { get; set; }
        public string ContractSellerName { get; set; }
        public int ContractBuyerId { get; set; }
        public string ContractBuyerName { get; set; }
        public string InvoiceNumber  { get; set; }
        public string InvoiceImgSrc  { get; set; }
        public decimal Amount  { get; set; }
        public DateTime ExpiryDate  { get; set; }
        public string Status  { get; set; }

        private static String lang = CommonConfiguration.SiteLanguage();
        CultureInfo culture = CultureInfo.CreateSpecificCulture(lang);
        VAHAEntities entity = null;

        public ContractObjects()
        {
            this.ContractId = 0;
            this.ContractOwnerId = 0;
            this.ContractSellerId = 0;
            this.ContractBuyerId = 0;
            this.InvoiceNumber = "";
            this.InvoiceImgSrc = "";
            this.Amount = 0;
            this.ExpiryDate = DateTime.MinValue;
            this.Status = "";
        }
        public ContractObjects(List<FormDataNameValue> formdatapair)
        {
            Dictionary<String, object> valuesDict = new Dictionary<string, object>();
            FormDataRetreiver.getFormData(formdatapair, out valuesDict);
            this.ContractId = 0;
            this.ContractOwnerId = 0;
            this.ContractSellerId = 0;
            this.ContractBuyerId = 0;
            this.InvoiceNumber = valuesDict.ContainsKey("InvoiceNumber") && valuesDict["InvoiceNumber"] != null ? (string)valuesDict["InvoiceNumber"] : "";
            this.InvoiceImgSrc = valuesDict.ContainsKey("InvoiceImgSrc") && valuesDict["InvoiceImgSrc"] != null ? (string)valuesDict["InvoiceImgSrc"] : "";
            this.Amount = 0;
            this.ExpiryDate = DateTime.MinValue;
            this.Status = valuesDict.ContainsKey("Status") && valuesDict["Status"] != null ? (string)valuesDict["Status"] : "";
        }
        public String[] Create()
        {
            try
            {
                String[] result = { "true", "", "" };
                entity = new VAHAEntities();
                Vaha_Contracts vahaContractTempObj = null;
                String requiredErrMsg = StringResources.getString(StringResources.RES_ERR, "GENERIC_REQUIRED_ERR", lang);
 
                vahaContractTempObj = new Vaha_Contracts();
                vahaContractTempObj.ContractOwnerId = this.ContractOwnerId;
                vahaContractTempObj.ContractSellerId = this.ContractSellerId;
                vahaContractTempObj.Amount = this.Amount;
                vahaContractTempObj.ContractBuyerId = this.ContractBuyerId;
                vahaContractTempObj.ExpiryDate = this.ExpiryDate;
                vahaContractTempObj.InvoiceImgSrc = this.InvoiceImgSrc;
                vahaContractTempObj.InvoiceNumber = this.InvoiceNumber;
                vahaContractTempObj.Status = this.Status;
                entity.Vaha_Contracts.Add(vahaContractTempObj);
                entity.SaveChanges();

                result[0] = "true";
                result[1] = "";
                result[2] = "";

                return result;
            }
            catch (Exception ex)
            {
                String[] result = new String[3];
                result[0] = "false";
                result[1] = "-2";
                result[2] = ex.Message;
                return result;
            }
        }
        public List<ContractObjects> GetContractsListBySellerID(int sellerID)
        {
            List<ContractObjects> tmpObjList = null;
            ContractObjects tmpObj = null;
            try
            {
                entity = new VAHAEntities();

                List<Vaha_Contracts> contractsObj = entity.Vaha_Contracts.Where(Contract => Contract.ContractSellerId == sellerID).ToList();

                if (contractsObj != null)
                {
                    tmpObjList = new List<ContractObjects>();

                    foreach (Vaha_Contracts item in contractsObj)
                    {
                        tmpObj = new ContractObjects();
                        tmpObj.Amount = item.Amount;
                        tmpObj.ContractBuyerId = item.ContractBuyerId;
                        tmpObj.ContractId = item.ContractId;
                        tmpObj.ContractOwnerId = item.ContractOwnerId;
                        tmpObj.ContractSellerId = item.ContractSellerId;
                        tmpObj.ExpiryDate = item.ExpiryDate;
                        tmpObj.InvoiceImgSrc = item.InvoiceImgSrc;
                        tmpObj.InvoiceNumber = item.InvoiceNumber;
                        tmpObj.Status = item.Status;
                        tmpObjList.Add(tmpObj);
                    }
                    return tmpObjList;
                }
                else
                {
                    tmpObjList = null;
                    return tmpObjList;
                }
            }
            catch (Exception ex)
            {
                tmpObjList = null;
                return tmpObjList;
            }
        }

        public List<ContractObjects> GetContractsListByBuyerID(int buyerId)
        {
            List<ContractObjects> tmpObjList = null;
            ContractObjects tmpObj = null;
            try
            {
                entity = new VAHAEntities();

                List<Vaha_Contracts> contractsObj = entity.Vaha_Contracts.Where(Contract => Contract.ContractBuyerId == buyerId).ToList();

                if (contractsObj != null)
                {
                    tmpObjList = new List<ContractObjects>();

                    foreach (Vaha_Contracts item in contractsObj)
                    {
                        tmpObj = new ContractObjects();
                        tmpObj.Amount = item.Amount;
                        tmpObj.ContractBuyerId = item.ContractBuyerId;
                        tmpObj.ContractId = item.ContractId;
                        tmpObj.ContractOwnerId = item.ContractOwnerId;
                        tmpObj.ContractSellerId = item.ContractSellerId;
                        tmpObj.ExpiryDate = item.ExpiryDate;
                        tmpObj.InvoiceImgSrc = item.InvoiceImgSrc;
                        tmpObj.InvoiceNumber = item.InvoiceNumber;
                        tmpObj.Status = item.Status;
                        tmpObjList.Add(tmpObj);
                    }
                    return tmpObjList;
                }
                else
                {
                    tmpObjList = null;
                    return tmpObjList;
                }
            }
            catch (Exception ex)
            {
                tmpObjList = null;
                return tmpObjList;
            }
        }

        public List<ContractObjects> GetContractsListByContractID(int contractID)
        {
            List<ContractObjects> tmpObjList = null;
            ContractObjects tmpObj = null;
            try
            {
                entity = new VAHAEntities();

                List<Vaha_Contracts> contractsObj = entity.Vaha_Contracts.Where(Contract => Contract.ContractId == contractID).ToList();

                if (contractsObj != null)
                {
                    foreach (Vaha_Contracts item in contractsObj)
                    {
                        tmpObj = new ContractObjects();
                        tmpObj.Amount = item.Amount;
                        tmpObj.ContractBuyerId = item.ContractBuyerId;
                        tmpObj.ContractId = item.ContractId;
                        tmpObj.ContractOwnerId = item.ContractOwnerId;
                        tmpObj.ContractSellerId = item.ContractSellerId;
                        tmpObj.ExpiryDate = item.ExpiryDate;
                        tmpObj.InvoiceImgSrc = item.InvoiceImgSrc;
                        tmpObj.InvoiceNumber = item.InvoiceNumber;
                        tmpObj.Status = item.Status;
                        tmpObjList.Add(tmpObj);
                    }
                    return tmpObjList;
                }
                else
                {
                    tmpObjList = null;
                    return tmpObjList;
                }
            }
            catch (Exception ex)
            {
                tmpObjList = null;
                return tmpObjList;
            }
        }
    }
    
}
