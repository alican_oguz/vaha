﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using VahaInetpub.Utility;

namespace VahaInetpub.Models
{
    public class PageDefinitonData
    {
        public string PageKey { get; set; }
        private string PageKeyInfo;
        private string PageKeyLink;
        public string PageTitle { get; set; }
        public string PageHeader { get; set; }
        public string PageHeaderInfo { get; set; }
        private List<PageBreadCrumbItem> BcItems;

        public PageDefinitonData()
        {
            String lang = CommonConfiguration.SiteLanguage();
            this.PageKey = "DefaultPage";
            this.PageKeyInfo = "DefaultPage" + "_Info";
            this.PageKeyLink = "DefaultPage" + "_Link";
            this.PageTitle = StringResources.getString(StringResources.RES_PAGE_TITLE, this.PageKey, lang);
            this.PageHeader = StringResources.getString(StringResources.RES_PAGE_HEADER, this.PageKey, lang);
            this.PageHeaderInfo = StringResources.getString(StringResources.RES_PAGE_HEADER, this.PageKeyInfo, lang);
            BcItems = new List<PageBreadCrumbItem>();
            initBreadCrumbData();
        }

        public PageDefinitonData(String pageKey, Boolean hasLink)
        {
            String lang = CommonConfiguration.SiteLanguage();
            this.PageKey = pageKey;
            this.PageKeyInfo = pageKey + "_Info";
            this.PageKeyLink = pageKey + "_Link";
            this.PageTitle = StringResources.getString(StringResources.RES_PAGE_TITLE, this.PageKey, lang);
            this.PageHeader = StringResources.getString(StringResources.RES_PAGE_HEADER, this.PageKey, lang);
            this.PageHeaderInfo = StringResources.getString(StringResources.RES_PAGE_HEADER, this.PageKeyInfo, lang);
            BcItems = new List<PageBreadCrumbItem>();
            initBreadCrumbData();
            this.AddBreadCrumbItem(pageKey, hasLink);
        }

        public PageDefinitonData(List<PageBreadCrumbItem> bcItems, String pageTitle, String pageHeder, String pageHeaderInfo)
        {
            this.BcItems = bcItems;
            this.PageTitle = pageTitle;
            this.PageHeader = pageHeder;
            this.PageHeaderInfo = pageHeaderInfo;
        }

        private void initBreadCrumbData()
        {
            String lang = CommonConfiguration.SiteLanguage();
            PageBreadCrumbItem homeItem = new PageBreadCrumbItem();
            homeItem.BcText = StringResources.getString(StringResources.RES_PAGE_HEADER, "HomePage", lang);
            homeItem.BcLink = StringResources.getString(StringResources.RES_PAGE_HEADER, "HomePage_Link", lang);
            BcItems.Add(homeItem);
        }

        public void AddBreadCrumbItem(String pageKey, Boolean hasLink)
        {
            String lang = CommonConfiguration.SiteLanguage();
            PageBreadCrumbItem tmpItem = new PageBreadCrumbItem();
            tmpItem.BcText = StringResources.getString(StringResources.RES_PAGE_HEADER, pageKey, lang);
            tmpItem.BcLink = !hasLink ? "javascript:void(0)" : StringResources.getString(StringResources.RES_PAGE_HEADER, pageKey + "_Link", lang);
            if (BcItems.Count == 1)
                BcItems.Add(tmpItem);
            else if (BcItems.Count > 1)
                BcItems.Insert(BcItems.Count - 1, tmpItem);
        }

        public void SaveBreadCrumbData(HttpSessionStateBase sess)
        {
            sess["BREADCRUMB_DATA"] = this.BcItems;
            sess["CURR_PAGE_TITLE"] = this.PageTitle;
            sess["CURR_PAGE_HEADER"] = this.PageHeader;
            sess["CURR_PAGE_HEADER_INFO"] = this.PageHeaderInfo;
        }

        public List<PageBreadCrumbItem> GetBreadCrumbItems()
        {
            return this.BcItems;
        }

        public bool isFirstBreadCrumbItem(PageBreadCrumbItem pgItem)
        {
            return pgItem.Equals(this.BcItems.ElementAt(0));
        }

        public bool isLastBreadCrumbItem(PageBreadCrumbItem pgItem)
        {
            return pgItem.Equals(this.BcItems.ElementAt(this.BcItems.Count-1));
        }

        public static PageDefinitonData GetBreadCrumbSessionData(HttpSessionStateBase sess)
        {
            PageDefinitonData sessionData = new PageDefinitonData();

            if (sess["BREADCRUMB_DATA"] == null || sess["CURR_PAGE_TITLE"] == null || sess["CURR_PAGE_HEADER"] == null || sess["CURR_PAGE_HEADER_INFO"] == null)
            {
                sessionData.SaveBreadCrumbData(sess);
                return sessionData;
            }
            else
            {
                List<PageBreadCrumbItem> bcItems = (List<PageBreadCrumbItem>)sess["BREADCRUMB_DATA"];
                String pageTitle = (String)sess["CURR_PAGE_TITLE"];
                String pageHeder = (String)sess["CURR_PAGE_HEADER"];
                String pageHeaderInfo = (String)sess["CURR_PAGE_HEADER_INFO"];
                sessionData = new PageDefinitonData(bcItems, pageTitle, pageHeder, pageHeaderInfo);
            }

            return sessionData;
        }

        public static void ClearBreadCrumbSessionData(HttpSessionStateBase sess)
        {
            sess.Remove("BREADCRUMB_DATA");
            sess.Remove("CURR_PAGE_TITLE");
            sess.Remove("CURR_PAGE_HEADER");
            sess.Remove("CURR_PAGE_HEADER_INFO");
        }

    }

    public class PageBreadCrumbItem
    {
        public string BcText { get; set; }
        public string BcLink { get; set; }

        public PageBreadCrumbItem()
        {
            this.BcText = "";
            this.BcLink = "javascript:void(0)";
        }
    }
}