﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace VahaInetpub.Models
{
    public class UserObjectSet
    {
        public List<UserObject> UserObjectList { get; set; }

        public UserObjectSet()
        {
            UserObjectList = new List<UserObject>();
        }

        public UserObjectSet Load()
        {
            try
            {
                List<Users> users = null;
                VAHAEntities entity = new VAHAEntities();

                users = entity.Users.Where(
                        user => user.Deleted != 1
                    ).OrderBy(user => user.Email).ToList<Users>();

                UserObjectList = new List<UserObject>();

                if (users != null)
                {
                    foreach (Users usrObj in users)
                    {
                        UserObject usrTmpObj = new UserObject();
                        usrTmpObj.Username = usrObj.Username;
                        usrTmpObj.Deleted = short.Parse(usrObj.Deleted.ToString());
                        usrTmpObj.Email = usrObj.Email;
                        usrTmpObj.Name = usrObj.Name;
                        usrTmpObj.Surname = usrObj.Surname;
                        usrTmpObj.MidName = usrObj.MidName;
                        usrTmpObj.Pwd = usrObj.Pwd;
                        usrTmpObj.Role = short.Parse(usrObj.Role.ToString());
                        usrTmpObj.ID = usrObj.Id;
                        UserObjectList.Add(usrTmpObj);
                    }

                }


            }
            catch (Exception ex)
            {
                UserObjectList = new List<UserObject>();
            }

            return this;
        }
    }
}