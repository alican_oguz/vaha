﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Security.Cryptography;
using System.Globalization;
using System.Web;
using VahaInetpub.Utility;
using System.Net;
using System.Net.Mail;
using System.Web.Configuration;

namespace VahaInetpub.Models
{
    public class TransactionsObject
    {
        public int TransactionId  { get; set; }
        public int ContractId  { get; set; }
        public int DestinationBankId  { get; set; }
        public int SourceBankId  { get; set; }
        public decimal BlockedAmount  { get; set; }
        public decimal PayableAmount  { get; set; }
        public DateTime TransctionExpDate  { get; set; }
        public string Status  { get; set; }
        public decimal SourceBankCommission  { get; set; }
        public decimal DestBankCommission { get; set; }

        private static String lang = CommonConfiguration.SiteLanguage();
        CultureInfo culture = CultureInfo.CreateSpecificCulture(lang);
        VAHAEntities entity = null;

        public TransactionsObject()
        {
            this.TransactionId = 0;
            this.ContractId = 0;
            this.DestinationBankId = 0;
            this.SourceBankId = 0;
            this.BlockedAmount = 0;
            this.PayableAmount = 0;
            this.TransctionExpDate = DateTime.MinValue;
            this.Status = "";
            this.SourceBankCommission = 0;
            this.DestBankCommission = 0;
        }

        public TransactionsObject(List<FormDataNameValue> formdatapair)
        {
            Dictionary<String, object> valuesDict = new Dictionary<string, object>();
            FormDataRetreiver.getFormData(formdatapair, out valuesDict);
            this.TransactionId = 0;
            this.ContractId = 0;
            this.DestinationBankId = 0;
            this.SourceBankId = 0;
            this.BlockedAmount = 0;
            this.PayableAmount = 0;
            this.Status = valuesDict.ContainsKey("Status") && valuesDict["Status"] != null ? (string)valuesDict["Status"] : "";
            this.SourceBankCommission = 0;
            this.DestBankCommission = 0;        
        }

        public List<TransactionsObject> GetTRansactionsByContractId(int contractId)
        {
            List<TransactionsObject> tmpObjList = null;
            TransactionsObject tmpObj = null;
            try
            {
                entity = new VAHAEntities();

                List<Vaha_Transactions> transactionObj = entity.Vaha_Transactions.Where(
                    Transaction => Transaction.ContractId == contractId).ToList();

                if (transactionObj != null)
                {
                    foreach (Vaha_Transactions item in transactionObj)
                    {
                        tmpObj = new TransactionsObject();
                        tmpObj.BlockedAmount = item.BlockedAmount;
                        tmpObj.ContractId = item.ContractId;
                        tmpObj.DestBankCommission = item.DestBankCommission.HasValue ? item.DestBankCommission.Value : 0;
                        tmpObj.DestinationBankId = item.DestinationBankId;
                        tmpObj.PayableAmount = item.PayableAmount;
                        tmpObj.SourceBankCommission = item.SourceBankCommission.HasValue ? item.SourceBankCommission.Value :0;
                        tmpObj.SourceBankId = item.SourceBankId;
                        tmpObj.Status = item.Status;
                        tmpObj.TransactionId = item.TransactionId;
                        tmpObj.TransctionExpDate = item.TransctionExpDate;
                        tmpObjList.Add(tmpObj);
                    }
                    return tmpObjList;
                }
                else
                {
                    tmpObjList = null;
                    return tmpObjList;
                }
            }
            catch (Exception ex)
            {
                tmpObjList = null;
                return tmpObjList;
            }
        }
        public List<TransactionsObject> GetTRansactionsByTransactionId(int transactionId)
        {
            List<TransactionsObject> tmpObjList = null;
            TransactionsObject tmpObj = null;
            try
            {
                entity = new VAHAEntities();

                List<Vaha_Transactions> transactionObj = entity.Vaha_Transactions.Where(
                    Transaction => Transaction.TransactionId == transactionId).ToList();

                if (transactionObj != null)
                {
                    foreach (Vaha_Transactions item in transactionObj)
                    {
                        tmpObj = new TransactionsObject();
                        tmpObj.BlockedAmount = item.BlockedAmount;
                        tmpObj.ContractId = item.ContractId;
                        tmpObj.DestBankCommission = item.DestBankCommission.HasValue ? item.DestBankCommission.Value : 0;
                        tmpObj.DestinationBankId = item.DestinationBankId;
                        tmpObj.PayableAmount = item.PayableAmount;
                        tmpObj.SourceBankCommission = item.SourceBankCommission.HasValue ? item.SourceBankCommission.Value : 0;
                        tmpObj.SourceBankId = item.SourceBankId;
                        tmpObj.Status = item.Status;
                        tmpObj.TransactionId = item.TransactionId;
                        tmpObj.TransctionExpDate = item.TransctionExpDate;
                        tmpObjList.Add(tmpObj);
                    }
                    return tmpObjList;
                }
                else
                {
                    tmpObjList = null;
                    return tmpObjList;
                }
            }
            catch (Exception ex)
            {
                tmpObjList = null;
                return tmpObjList;
            }
        }
        public List<TransactionsObject> GetTRansactionsByStatus(string status)
        {
            List<TransactionsObject> tmpObjList = null;
            TransactionsObject tmpObj = null;
            try
            {
                entity = new VAHAEntities();

                List<Vaha_Transactions> transactionObj = entity.Vaha_Transactions.Where(
                    Transaction => Transaction.Status == status).ToList();

                if (transactionObj != null)
                {
                    foreach (Vaha_Transactions item in transactionObj)
                    {
                        tmpObj = new TransactionsObject();
                        tmpObj.BlockedAmount = item.BlockedAmount;
                        tmpObj.ContractId = item.ContractId;
                        tmpObj.DestBankCommission = item.DestBankCommission.HasValue ? item.DestBankCommission.Value : 0;
                        tmpObj.DestinationBankId = item.DestinationBankId;
                        tmpObj.PayableAmount = item.PayableAmount;
                        tmpObj.SourceBankCommission = item.SourceBankCommission.HasValue ? item.SourceBankCommission.Value : 0;
                        tmpObj.SourceBankId = item.SourceBankId;
                        tmpObj.Status = item.Status;
                        tmpObj.TransactionId = item.TransactionId;
                        tmpObj.TransctionExpDate = item.TransctionExpDate;
                        tmpObjList.Add(tmpObj);
                    }
                    return tmpObjList;
                }
                else
                {
                    tmpObjList = null;
                    return tmpObjList;
                }
            }
            catch (Exception ex)
            {
                tmpObjList = null;
                return tmpObjList;
            }
        }
    }
    
}
