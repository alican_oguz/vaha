﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Security.Cryptography;
using System.Globalization;
using System.Web;
using VahaInetpub.Utility;
using System.Net;
using System.Net.Mail;
using System.Web.Configuration;

namespace VahaInetpub.Models
{
    public class BanksObject
    {
        public int BankId { get; set; }
        public string BankName { get; set; }

        private static String lang = CommonConfiguration.SiteLanguage();
        CultureInfo culture = CultureInfo.CreateSpecificCulture(lang);
        VAHAEntities entity = null;

        public BanksObject()
        {
            this.BankId = 0;
            this.BankName = "";
        }
        public BanksObject(List<FormDataNameValue> formdatapair)
        {
            Dictionary<String, object> valuesDict = new Dictionary<string, object>();
            FormDataRetreiver.getFormData(formdatapair, out valuesDict);
            this.BankId = 0;
            this.BankName = valuesDict.ContainsKey("BankName") && valuesDict["BankName"] != null ? (string)valuesDict["BankName"] : "";
        }
        public List<BanksObject> GetBankList()
        {
            List<BanksObject> tmpObjList = null;
            BanksObject tmpObj = null;
            try
            {
                entity = new VAHAEntities();

                List<Vaha_Banks> bankSmeObj = entity.Vaha_Banks.ToList();

                if (bankSmeObj != null)
                {
                    foreach (Vaha_Banks item in bankSmeObj)
                    {
                        tmpObj = new BanksObject();
                        tmpObj.BankId = item.BankId;
                        tmpObj.BankName = item.BankName;
                        tmpObjList.Add(tmpObj);
                    }
                    return tmpObjList;
                }
                else
                {
                    tmpObjList = null;
                    return tmpObjList;
                }
            }
            catch (Exception ex)
            {
                tmpObjList = null;
                return tmpObjList;
            }
        }
    }
    
}
