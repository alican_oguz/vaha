﻿namespace VahaInetpub.Models
{
    public class UserSessionData
    {
        public string UserEmail { get; set; }
        public string Username { get; set; }
        public string Name { get; set; }
        public string MidName { get; set; }
        public string Surname { get; set; }
        public string Status { get; set; }
        public string AuthToken { get; set; }
        public string Role { get; set; }
        public int ID { get; set; }

        public UserSessionData()
        {
            this.Username = "";
            this.UserEmail = "";
            this.Name = "";
            this.MidName = "";
            this.Status = "";
            this.Surname = "";
            this.AuthToken = "";
            this.Role = "";
            this.ID = 0;
        }
    }


}