﻿using System;
using System.Collections.Generic;
using System.Web;
using VahaInetpub.Utility;

namespace VahaInetpub.Models
{
    public class SideBarMenuData
    {
        public string MenuHeader;
        public List<SideBarMenuNode> MenuNodes {get; set;}

        public SideBarMenuData()
        {
            String lang = CommonConfiguration.SiteLanguage();
            this.MenuHeader = StringResources.getString(StringResources.RES_LABEL, "SIDEBAR_MENU_HEADER", lang);
            this.MenuNodes = new List<SideBarMenuNode>();
        }

        public static SideBarMenuData GenerateMenu()
        {
            String lang = CommonConfiguration.SiteLanguage();
            SideBarMenuData menu = new SideBarMenuData();

            
            UserSessionData user = (UserSessionData)HttpContext.Current.Session["UserSessionData"];

            //Kontrol yönetimi
            SideBarMenuNode nodeContratManagement = SideBarMenuNode.Create(SideBarMenuData.GetMenuDisp("CONTRAT_MANAGEMENT", lang), "fa fa-money");
            SideBarMenuItem nodeContratManagement_item1 = SideBarMenuItem.CreateLeaf(SideBarMenuData.GetMenuDisp("CONTRAT_LIST", lang), "fa fa-check-square-o", "/contract/list");
            SideBarMenuItem nodeContratManagement_item2 = SideBarMenuItem.CreateLeaf(SideBarMenuData.GetMenuDisp("CONTRAT_ADD", lang), "fa fa-check-square-o", "/contract/add");
            nodeContratManagement.AddNodeItem(nodeContratManagement_item1);
            nodeContratManagement.AddNodeItem(nodeContratManagement_item2);
            menu.MenuNodes.Add(nodeContratManagement);

            //Akış yönetimi
            SideBarMenuNode nodeCashFlowManagement = SideBarMenuNode.Create(SideBarMenuData.GetMenuDisp("CASH_FLOW_MANAGEMENT", lang), "fa fa-line-chart");
            SideBarMenuItem nodeCashFlowManagement_item1 = SideBarMenuItem.CreateLeaf(SideBarMenuData.GetMenuDisp("CASHS_LIST", lang), "fa fa-money", "/cashflw/list");
            SideBarMenuItem nodeCashFlowManagement_item2 = SideBarMenuItem.CreateLeaf(SideBarMenuData.GetMenuDisp("ACCOUNT_LIST", lang), "fa fa-area-chart", "/account/list");
            nodeCashFlowManagement.AddNodeItem(nodeCashFlowManagement_item1);
            nodeCashFlowManagement.AddNodeItem(nodeCashFlowManagement_item2);
            menu.MenuNodes.Add(nodeCashFlowManagement);

            /* Kullanıcı Yönetimi */
            SideBarMenuNode nodeUserManagement = SideBarMenuNode.Create(SideBarMenuData.GetMenuDisp("USER_MANAGEMENT", lang), "fa fa-user");
            SideBarMenuItem nodeUserManagement_item1 = SideBarMenuItem.CreateLeaf(SideBarMenuData.GetMenuDisp("USERS_LIST", lang), "fa fa-users", "/usr/list");
            nodeUserManagement.AddNodeItem(nodeUserManagement_item1);
            menu.MenuNodes.Add(nodeUserManagement);

            return menu;
        }

        private static String GetMenuDisp(String key, String lang)
        {
            return StringResources.getString(StringResources.RES_MENU_ITEMS, key, lang);
        }
    }

    public class SideBarMenuNode
    {
        public string IconClass;
        public string ItemDisp;
        public List<SideBarMenuItem> NodeItems;

        public SideBarMenuNode()
        {
            this.IconClass = "fa fa-circle-o";
            this.ItemDisp = "N/A MenuNode";
            this.NodeItems = new List<SideBarMenuItem>();
        }

        public void AddNodeItem(SideBarMenuItem item)
        {
            this.NodeItems.Add(item);
        }

        public static SideBarMenuNode Create(string itemDisp, string iconClass)
        {
            SideBarMenuNode retItem = new SideBarMenuNode();

            retItem.ItemDisp = itemDisp;
            retItem.IconClass = iconClass.Equals("")?retItem.IconClass:iconClass;
            retItem.NodeItems = new List<SideBarMenuItem>();

            return retItem;
        }

    }

    public class SideBarMenuItem
    {
        public bool IsLeaf;
        public MenuLeafItem Item;
        public string IconClass;
        public string ItemDisp;
        public List<SideBarMenuItem> LeafItems;

        public SideBarMenuItem()
        {
            this.IsLeaf = false;
            this.Item = new MenuLeafItem();
            this.IconClass = "fa fa-circle-o";
            this.ItemDisp = "N/A MenuItem";
            this.LeafItems = new List<SideBarMenuItem>();
        }

        public void AddLeaf(SideBarMenuItem item)
        {
            this.LeafItems.Add(item);
        }

        public static SideBarMenuItem CreateLeaf(string itemDisp, string iconClass, string href)
        {
            SideBarMenuItem retItem = new SideBarMenuItem();

            retItem.Item = MenuLeafItem.Create(itemDisp, iconClass, href);
            retItem.IsLeaf = true;
            retItem.IconClass = "";
            retItem.ItemDisp = "";
            retItem.LeafItems = null;

            return retItem;
        }

        public static SideBarMenuItem Create(string itemDisp, string iconClass)
        {
            SideBarMenuItem retItem = new SideBarMenuItem();

            retItem.Item = null;
            retItem.IsLeaf = false;
            retItem.IconClass = iconClass.Equals("")?retItem.IconClass:iconClass;
            retItem.ItemDisp = itemDisp;
            retItem.LeafItems = new List<SideBarMenuItem>();

            return retItem;
        }
    }

    public class MenuLeafItem
    {
        public string Href;
        public string IconClass;
        public string ItemDisp;

        public MenuLeafItem()
        {
            this.Href = "javascript:void(0)";
            this.IconClass = "fa fa-circle-o";
            this.ItemDisp = "N/A LeafItem";
        }

        public static MenuLeafItem Create(string itemDisp, string iconClass, string href)
        {
            MenuLeafItem retItem = new MenuLeafItem();
            retItem.ItemDisp = itemDisp;
            retItem.IconClass = iconClass.Equals("") ? retItem.IconClass : iconClass;
            retItem.Href = href;
            return retItem;
        }
    }
}