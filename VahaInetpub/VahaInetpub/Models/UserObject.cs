﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using VahaInetpub.Utility;

namespace VahaInetpub.Models
{
    public class UserObject
    {
        public string Username { get; set; }
        public string Name { get; set; }
        public string MidName { get; set; }
        public string Surname { get; set; }
        public string Pwd { get; set; }
        public string Email { get; set; }
        public short Status { get; set; }
        public short Role { get; set; }
        public Int64 LastLoginDateTime { get; set; }
        public short Deleted { get; set; }
        public String SessionParam { get; set; }
        public int ID { get; set; }

        private static String lang = CommonConfiguration.SiteLanguage();
        CultureInfo culture = CultureInfo.CreateSpecificCulture(lang);
        VAHAEntities entity = null;

        public UserObject()
        {
            this.Username = "";
            this.Name = "";
            this.MidName = "";
            this.Surname = "";
            this.Pwd = "";
            this.Email = "";
            this.Status = 0;
            this.Role = 0;
            this.LastLoginDateTime = 0;
            this.Deleted = 0;
            this.SessionParam = "";
        }

        public UserObject(List<FormDataNameValue> formdatapair)
        {
            Dictionary<String, object> valuesDict = new Dictionary<string, object>();
            FormDataRetreiver.getFormData(formdatapair, out valuesDict);
            string sRole = valuesDict.ContainsKey("Role") && valuesDict["Role"] != null ? (string)valuesDict["Role"] : "";
            this.Username = valuesDict.ContainsKey("Username") && valuesDict["Username"] != null ? (string)valuesDict["Username"] : "";
            this.Name = valuesDict.ContainsKey("Name") && valuesDict["Name"] != null ? (string)valuesDict["Name"] : "";
            this.MidName = valuesDict.ContainsKey("MidName") && valuesDict["MidName"] != null ? (string)valuesDict["MidName"] : "";
            this.Surname = valuesDict.ContainsKey("Surname") && valuesDict["Surname"] != null ? (string)valuesDict["Surname"] : "";
            this.Pwd = valuesDict.ContainsKey("Password") && valuesDict["Password"] != null ? (string)valuesDict["Password"] : "";
            this.Email = valuesDict.ContainsKey("Email") && valuesDict["Email"] != null ? (string)valuesDict["Email"] : "";
            this.Status = 0;
            this.Role = short.Parse(sRole);
            this.LastLoginDateTime = 0;
            this.Deleted = 0;
            this.SessionParam = valuesDict.ContainsKey("refcode") ? (string)valuesDict["refcode"] : "";
        }

        public String[] Create()
        {
            try
            {
                String[] result = { "true", "", "" };
                entity = new VAHAEntities();
                Users userTmpObj = null;
                List<Users> userObj = null;
                String requiredErrMsg = StringResources.getString(StringResources.RES_ERR, "GENERIC_REQUIRED_ERR", lang);
                if (this.Name.Equals(""))
                {
                    result[0] = "false";
                    result[1] = "-1";
                    result[2] = String.Format(requiredErrMsg, "UserName");
                }
                else if (this.Surname.Equals(""))
                {
                    result[0] = "false";
                    result[1] = "-1";
                    result[2] = String.Format(requiredErrMsg, "UserSurname");
                }
                else if (this.Pwd.Equals(""))
                {
                    result[0] = "false";
                    result[1] = "-1";
                    result[2] = String.Format(requiredErrMsg, "UserPassword");
                }
                else if (this.Email.Equals(""))
                {
                    result[0] = "false";
                    result[1] = "-1";
                    result[2] = String.Format(requiredErrMsg, "Email");
                }

                if (result[0].Equals("false"))
                    return result;


                String userPassword = StringResources.MD5Hash(this.Pwd);
                String upperMail = this.Email.ToUpper(culture);
                String lowerMail = this.Email.ToLower(culture);

                userObj = entity.Users.Where(
                            user => (
                                        user.Email.Equals(this.Email) ||
                                        user.Email.Equals(upperMail) ||
                                        user.Email.Equals(lowerMail)

                                     )
                                        &&
                                        user.Deleted != 1
                            ).ToList<Users>();

                if (userObj.Count > 0)
                {
                    foreach (var item in userObj)
                    {
                        if (item.Email == this.Email)
                        {

                            String strInvEMail = String.Format(StringResources.getString(StringResources.RES_ERR, "USER_INV_EMAIL", lang), this.Email);
                            result[0] = "false";
                            result[1] = "-1";
                            result[2] = strInvEMail;
                        }
                    }


                }

                else
                {

                    userTmpObj = new Users();
                    userTmpObj.Username = this.Username;
                    userTmpObj.Name = this.Name;
                    userTmpObj.MidName = this.MidName;
                    userTmpObj.Surname = this.Surname;
                    userTmpObj.Pwd = userPassword;
                    userTmpObj.Email = this.Email;
                    userTmpObj.Deleted = this.Deleted;
                    userTmpObj.LastLoginDateTime = 0;
                    userTmpObj.Role = this.Role;

                    entity.Users.Add(userTmpObj);
                    entity.SaveChanges();

                    result[0] = "true";
                    result[1] = "";
                    result[2] = "";


                }

                return result;
            }
            catch (Exception ex)
            {
                String[] result = new String[3];
                result[0] = "false";
                result[1] = "-2";
                result[2] = ex.Message;
                return result;
            }
        }

        public String[] UpdateUserInfo(String userMail, UserObject srcUserObj)
        {
            try
            {
                String[] result = { "true", "", "" };

                String requiredErrMsg = StringResources.getString(StringResources.RES_ERR, "GENERIC_REQUIRED_ERR", lang);
                if (userMail.Equals(""))
                {
                    result[0] = "false";
                    result[1] = "-1";
                    result[2] = String.Format(requiredErrMsg, "UserName");
                    return result;
                }


                entity = new VAHAEntities();
                String upperMail = userMail.ToUpper(culture);
                String lowerMail = userMail.ToLower(culture);
                this.Email = srcUserObj.Email;
                Users userObj =
                    entity.Users
                        .Where(
                            user => (
                                        user.Email.Equals(userMail) ||
                                        user.Email.Equals(upperMail) ||
                                        user.Email.Equals(lowerMail)
                                    ) &&
                                        user.Deleted != 1
                        ).SingleOrDefault();

                if (userObj != null)
                {
                    Users mailCheck =
                    entity.Users
                        .Where(
                            mail => (
                                mail.Deleted != 1 && mail.Email.Equals(this.Email) && !mail.Email.Equals("")
                                    )
                        ).SingleOrDefault();
                    if (mailCheck != null && userMail != srcUserObj.Email)
                    {

                        String strInvEMail = String.Format(StringResources.getString(StringResources.RES_ERR, "USER_INV_EMAIL", lang), srcUserObj.Email);
                        result[0] = "false";
                        result[1] = "-1";
                        result[2] = strInvEMail;
                        return result;
                    }
                    userObj.Username = srcUserObj.Username;
                    userObj.Name = srcUserObj.Name;
                    userObj.MidName = srcUserObj.MidName;
                    userObj.Surname = srcUserObj.Surname;
                    userObj.Email = srcUserObj.Email;
                    userObj.Role = srcUserObj.Role;
                    entity.SaveChanges();

                    result[0] = "true";
                    result[1] = "";
                    result[2] = "";

                }
                else
                {
                    result[0] = "false";
                    result[1] = "-1";
                    result[2] = StringResources.getString(StringResources.RES_ERR, "USER_NOT_FOUND", lang);
                }

                return result;
            }
            catch (Exception ex)
            {
                String[] result = new String[3];
                result[0] = "false";
                result[1] = "-4";
                result[2] = ex.Message;
                return result;
            }
        }

        public String[] Delete()
        {
            try
            {
                String[] result = { "true", "", "" };

                entity = new VAHAEntities();
                String upperEmail = this.Email.ToUpper(culture);
                String lowerEmail = this.Email.ToLower(culture);

                Users userObj = entity.Users.Where(
                    user =>
                        (user.Email.Equals(this.Email) ||
                            user.Email.Equals(upperEmail) ||
                            user.Email.Equals(lowerEmail)
                        ) && user.Deleted != 1
                    ).SingleOrDefault();

                if (userObj != null)
                {
                    userObj.Deleted = 1;
                    entity.SaveChanges();
                    result[0] = "true";
                    result[1] = "";
                    result[2] = "";
                }
                else
                {
                    result[0] = "false";
                    result[1] = "-1";
                    result[2] = StringResources.getString(StringResources.RES_ERR, "USER_NOT_FOUND", lang);
                }

                return result;
            }
            catch (Exception ex)
            {
                String[] result = { "false", "-2", ex.Message };
                return result;
            }
        }

        public String[] Login(out UserObject loginUserObject)
        {
            try
            {
                loginUserObject = new UserObject();
                String[] result = new String[3];

                entity = new VAHAEntities();
                String userPassword = StringResources.MD5Hash(this.Pwd);
                String upperMail = this.Email.ToUpper(culture);
                String lowerMail = this.Email.ToLower(culture);
                Users loginUser;

                loginUser = entity.Users.Where(
                    user => (
                        user.Email.Equals(this.Email) ||
                        user.Email.Equals(upperMail) ||
                        user.Email.Equals(lowerMail))
                        &&
                        user.Pwd == userPassword &&
                        user.Deleted == 0
                    ).FirstOrDefault();

                if (loginUser != null)
                {
                    loginUserObject = new UserObject();


                    loginUser.LastLoginDateTime = StringResources.GetLongCurrentDateTime();
                    entity.SaveChanges();

                    loginUserObject = new UserObject();
                    loginUserObject.Username = loginUser.Username;
                    loginUserObject.Deleted = short.Parse(loginUser.Deleted.ToString());
                    loginUserObject.Email = loginUser.Email;
                    loginUserObject.Name = loginUser.Name;
                    loginUserObject.MidName = loginUser.MidName;
                    loginUserObject.Surname = loginUser.Surname;
                    loginUserObject.Pwd = loginUser.Pwd;
                    loginUserObject.Role = (short)loginUser.Role;
                    loginUserObject.ID = loginUser.Id;

                    result[0] = "true";
                    result[1] = "";
                    result[2] = "";

                }
                else
                {
                    result[0] = "false";
                    result[1] = "-1";
                    result[2] = "Kullanıcı adı veya şifre yanlış";
                }


                return result;
            }
            catch (Exception ex)
            {
                String[] result = new String[3];
                result[0] = "false";
                result[1] = "-10";
                result[2] = ex.Message;

                loginUserObject = new UserObject();
                return result;
            }
        }

        public UserObject GetUserByUserName(String userEmail)
        {
            UserObject tmpObj = null;
            try
            {
                entity = new VAHAEntities();
                String upperEmail = userEmail.ToUpper(culture);
                String lowerEmail = userEmail.ToLower(culture);

                Users userObj = entity.Users.Where(
                    user => (user.Email.Equals(this.Email) ||
                            user.Email.Equals(upperEmail) ||
                            user.Email.Equals(lowerEmail)) &&
                            user.Deleted != 1
                        ).SingleOrDefault();

                if (userObj != null)
                {
                    tmpObj = new UserObject();
                    tmpObj.Username = userObj.Username;
                    tmpObj.Name = userObj.Name;
                    tmpObj.Email = userObj.Email;
                    tmpObj.MidName = userObj.MidName;
                    tmpObj.Surname = userObj.Surname;
                    tmpObj.Role = short.Parse(userObj.Role.ToString());
                    tmpObj.Deleted = short.Parse(userObj.Deleted.ToString());
                    return tmpObj;
                }
                else
                {
                    tmpObj = null;
                    return tmpObj;
                }
            }
            catch (Exception ex)
            {
                tmpObj = null;
                return tmpObj;
            }
        }

        public void GenerateSessionParam()
        {
            this.SessionParam = "";
            String param = SessionParamsUtil.createParameter("UserObject.UserEmail", this.Email);
            this.SessionParam = SessionParamsUtil.addToParameterTable(param);
        }

        public Boolean isLoggedInUser()
        {
            UserSessionData userInfoItem = HttpContext.Current.Session["UserSessionData"] != null ? (UserSessionData)HttpContext.Current.Session["UserSessionData"] : new UserSessionData();
            if (userInfoItem.UserEmail.Equals(this.Email))
                return true;
            else
                return false;
        }
    }
}