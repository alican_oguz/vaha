﻿using System;
using System.Globalization;
using VahaInetpub.Utility;

namespace VahaInetpub.Models
{
    public class CashFlowObject
    {
        public string BuySell { get; set; }
        public string Amount { get; set; }
        public string Date { get; set; }
        public string Status { get; set; }
        public string Description { get; set; }


        private static String lang = CommonConfiguration.SiteLanguage();
        CultureInfo culture = CultureInfo.CreateSpecificCulture(lang);
        VAHAEntities entity = null;

        public CashFlowObject()
        {
        }
    }
}