﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using VahaInetpub.Utility;

namespace VahaInetpub.Models
{
    public class ModalContent
    {
        public string ModalId { get; set; }
        public string ModalTitle { get; set; }
        public String ModalIcon { get; set; }
        public string Body { get; set; }
        public string Btn1Text { get; set; }
        public string Btn1Class { get; set; }
        public string Btn1Href { get; set; }
        public string Btn1Loading { get; set; }
        public string Btn2Text { get; set; }
        public string Btn2Class { get; set; }
        public string Btn2Href { get; set; }
        public string Btn2Loading { get; set; }
        public Boolean isBtn1 { get; set; }
        public Boolean isBtn2 { get; set; }

        public ModalContent()
        {
        }

        public int Create(ErrorModalData errorModal)
        {
            ErrorModalData modalData = errorModal;

            if (modalData != null)
            {
                this.ModalId = modalData.ModalId;
                this.ModalTitle = modalData.Title;
                this.ModalIcon = modalData.Icon;
                this.Body = modalData.Body;

                if (!modalData.Btn1Class.Equals(""))
                    this.Btn1Class = "btn-" + modalData.Btn1Class;

                if (!modalData.Btn1Text.Equals(""))
                {
                    this.isBtn1 = true;
                    this.Btn1Text = modalData.Btn1Text;
                    this.Btn1Loading = modalData.Btn1Loading;
                }
                else
                    this.isBtn1 = false;

                if (modalData.Btn1Href.Equals("dismiss"))
                {
                    this.Btn1Href = "$('#" + this.ModalId + "').modal('hide')";
                }
                else if (!modalData.Btn1Href.Equals(""))
                {
                    this.Btn1Href = modalData.Btn1Href;
                }
                else
                {
                    this.Btn1Href = "javascript:void(0)";
                }

                if (!modalData.Btn2Class.Equals(""))
                    this.Btn2Class = "btn-" + modalData.Btn2Class;

                if (!modalData.Btn2Text.Equals(""))
                {
                    this.isBtn2 = true;
                    this.Btn2Text = modalData.Btn2Text;
                    this.Btn2Loading = modalData.Btn2Loading;
                }
                else
                    this.isBtn2 = false;

                if (modalData.Btn2Href.Equals("dismiss"))
                {
                    this.Btn2Href = "$('#" + this.ModalId + "').modal('hide')";
                }
                else if (!modalData.Btn2Href.Equals(""))
                {
                    this.Btn2Href = modalData.Btn2Href;
                }
                else
                {
                    this.Btn2Href = "javascript:void(0)";
                }

                return 1;
            }
            else
            {
                String lang = CommonConfiguration.SiteLanguage();
                this.ModalId = "";
                this.ModalIcon = "F";
                this.ModalTitle = StringResources.getString(StringResources.RES_ERR, "DEFAULT_ERR_MODAL_TITLE", lang);
                this.Body = StringResources.getString(StringResources.RES_ERR, "DEFAULT_ERR_MODAL_MSG", lang);
                this.isBtn1 = true;
                this.Btn1Class = "btn-default";
                this.Btn1Text = StringResources.getString(StringResources.RES_LABEL, "LBL_CLOSE", lang);
                this.Btn1Href = "";
                this.Btn1Loading = "";
                this.isBtn2 = false;
                return 0;
            }
        }

        public void CreateDefaultErrorModal(String errorcode, String errordesc)
        {
            String lang = CommonConfiguration.SiteLanguage();
            String errcode = errorcode != null ? errorcode : "1";
            String errdesc = errordesc != null ? errordesc : "";

            if (errcode.Equals("1"))
            {
                this.ModalTitle = StringResources.getString(StringResources.RES_ERR, "DEFAULT_ERR_MODAL_TITLE", lang);
                this.ModalIcon = "F";
                this.Body = StringResources.getString(StringResources.RES_ERR, "DEFAULT_ERR_MODAL_MSG", lang);
                if (!errdesc.Equals(""))
                    this.Body += "<br \\><br \\><b>{" + errdesc + "}</b>";
                this.isBtn1 = true;
                this.Btn1Class = "btn-default";
                this.Btn1Text = StringResources.getString(StringResources.RES_LABEL, "LBL_CLOSE", lang);
                this.Btn1Href = "";
                this.Btn1Loading = "";
                this.isBtn2 = false;
            }
        }
    }
}