﻿using System.Web;
using System.Web.Optimization;
using VahaInetpub.Utility;
namespace VahaInetpub
{
    public class BundleConfig
    {
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                    "~/Common/script/tools/jquery-1.11.3.min.js",
                    "~/Common/script/tools/jquery.validate.min.js",
                    "~/Common/script/tools/jquery-ui.js",
                    "~/Common/script/tools/messages_tr.js",
                    "~/Common/script/tools/additional-methods.js"));


            bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include(
                "~/Common/bootstrap/js/bootstrap.min.js"));

            bundles.Add(new ScriptBundle("~/bundles/adminlte").Include(
                "~/Common/adminlte/js/app.min.js",
                "~/Common/plugins/slimScroll/jquery.slimscroll.min.js",
                "~/Common/plugins/fastclick/fastclick.min.js",
                "~/Common/adminlte/js/demo.js",
                  "~/Common/script/tools/Moment.js",
                    "~/Common/script/tools/BoxRefresh.js",
                      "~/Common/script/tools/BoxWidget.js",
                        "~/Common/script/tools/Chart.js",
                          "~/Common/script/tools/ControlSidebar.js",
                           "~/Common/script/tools/DirectChat.js",
                            "~/Common/script/tools/PushMenu.js",
                             "~/Common/script/tools/TodoList.js",
                              "~/Common/script/tools/Tree.js",
                            "~/Common/script/tools/Moment.js",
                             "~/Common/script/tools/jquery-jvectormap-1.2.2.min.js",
                              "~/Common/script/tools/jquery-jvectormap-usa-en.js",
                               "~/Common/script/tools/jquery-jvectormap-world-mill-en.js",
                                "~/Common/script/tools/Pace.js",
                                 "~/Common/script/tools/Pace.min.js",
                                  "~/Common/script/tools/bootstrapdatepicker.min.js",
                                 "~/Common/script/tools/daterangepicker.js",
            "~/Common/script/tools/Chart.js"));

            bundles.Add(new ScriptBundle("~/bundles/tools").Include(
                "~/Common/script/tools/delete.js",
                "~/Common/script/tools/tools.js"));


            bundles.Add(new ScriptBundle("~/bundles/ckfinder").Include(
                "~/Common/plugins/ckfinder/ckfinder.js"));

            bundles.Add(new StyleBundle("~/Content/css").Include(
                "~/Common/bootstrap/css/bootstrap.min.css",
                "~/Common/style/font-awesome.min.css",
                "~/Common/adminlte/css/AdminLTE.min.css",
                "~/Common/adminlte/css/skins/skin-blue.min.css",
                "~/Common/style/ionicons.min.css",
                  "~/Common/script/tools/pace.min.css",
                    "~/Common/script/tools/pace.js",
                      "~/Common/script/tools/jquery-jvectormap-1.2.2.css",
                      "~/Common/script/tools/daterangepicker.css",
                                     "~/Common/script/tools/bootstrap-datepicker.min.css",
                "~/Common/style/layout/site.css"));

            bundles.Add(new StyleBundle("~/tools/ui").Include(
                "~/Common/style/jquery-ui.css"));

            #region plugins

            bundles.Add(new StyleBundle("~/plugins/css/iCheck").Include(
                "~/Common/plugins/iCheck/square/blue.css"));

            bundles.Add(new ScriptBundle("~/plugins/js/iCheck").Include(
                "~/Common/plugins/iCheck/iCheck.min.js"));

            bundles.Add(new StyleBundle("~/plugins/css/datatables").Include(
                "~/Common/plugins/datatables/dataTables.bootstrap.css"));

            bundles.Add(new ScriptBundle("~/plugins/js/dataTables").Include(
                "~/Common/plugins/datatables/jquery.dataTables.min.js",
                "~/Common/plugins/datatables/dataTables.bootstrap.min.js"));

            bundles.Add(new StyleBundle("~/plugins/css/btWYSIHTML").Include(
                "~/Common/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css"));

            bundles.Add(new ScriptBundle("~/plugins/js/btWYSIHTML").Include(
                "~/Common/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js"));

            bundles.Add(new ScriptBundle("~/bundles/smooth").Include(
               "~/Common/script/tools/smooth.js",
               "~/Common/script/pages/DraftMapDrawSlopeItems.js"));

            #endregion


            #region Pages

            bundles.Add(new ScriptBundle("~/pages/js/LoginPage").Include(
                "~/Common/script/pages/LoginPage.js"));

            bundles.Add(new ScriptBundle("~/pages/js/RememberPwdPage").Include(
                "~/Common/script/pages/RememberPwdPage.js"));

            bundles.Add(new ScriptBundle("~/pages/js/UserMustChangePwdPage").Include(
                "~/Common/script/pages/UserMustChangePwdPage.js"));

            bundles.Add(new ScriptBundle("~/pages/js/UserAdd").Include(
                "~/Common/script/pages/UserAdd.js"));

            bundles.Add(new ScriptBundle("~/pages/js/UserEdit").Include(
                "~/Common/script/pages/UserEdit.js"));

            bundles.Add(new ScriptBundle("~/pages/js/CreateContract").Include(
              "~/Common/script/pages/CreateContract.js"));

            #endregion

        }
    }
}
