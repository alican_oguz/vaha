﻿using System.Web.Mvc;
using System.Web.Routing;

namespace VahaInetpub
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            #region LoginController

            routes.MapRoute(
                name: "Login",
                url: "login",
                defaults: new { controller = "Login", action = "LoginPage" }
            );

            routes.MapRoute(
               name: "mustchangepwd",
               url: "mustchangepwd",
               defaults: new { controller = "Login", action = "UserMustChangePwdPage" }
           );

            routes.MapRoute(
                name: "RememberPwd",
                url: "pwdre",
                defaults: new { controller = "Login", action = "RememberPwdPage" }
            );

            #endregion

            #region users management

            routes.MapRoute(
                name: "UserList",
                url: "usr/list",
                defaults: new { controller = "UserMng", action = "UserList" }
            );

            routes.MapRoute(
                name: "UserEdit",
                url: "usr/edit/{refcode}",
                defaults: new { controller = "UserMng", action = "UserEdit" }
            );

            routes.MapRoute(
                name: "UserEditGet",
                url: "edit/usr/get",
                defaults: new { controller = "UserMng", action = "DoGetUserEdit" }
            );

            routes.MapRoute(
                name: "UserEditProcess",
                url: "process/usr/edit",
                defaults: new { controller = "UserMng", action = "DoUserEdit" }
            );

            routes.MapRoute(
                name: "UserAdd",
                url: "usr/add",
                defaults: new { controller = "UserMng", action = "UserAdd" }
            );

            routes.MapRoute(
               name: "UserDelete",
               url: "process/usr/delete/{refcode}",
               defaults: new { controller = "UserMng", action = "DeleteUser" }
            );

            routes.MapRoute(
               name: "UserBlockLogin",
               url: "process/usr/block/{refcode}",
               defaults: new { controller = "UserMng", action = "BlockUser" }
            );

            routes.MapRoute(
               name: "UserUnBlockLogin",
               url: "process/usr/unblock/{refcode}",
               defaults: new { controller = "UserMng", action = "UnBlockUser" }
            );

            routes.MapRoute(
               name: "UserRenewPed",
               url: "process/usr/renewpwd/{refcode}",
               defaults: new { controller = "UserMng", action = "RenewPwd" }
            );
            routes.MapRoute(
              name: "RedirectPage",
              url: "login/signwithapi",
              defaults: new { controller = "Login", action = "RedirectPage" }
           );
            routes.MapRoute(
               name: "CreateContract",
               url: "contract/add",
               defaults: new { controller = "Home", action = "CreateContract" }
           );
            #endregion

            #region CONTRACT
            routes.MapRoute(
               name: "Contracts",
               url: "contract/list",
               defaults: new { controller = "FlowMng", action = "ContractList" }
           );
            #endregion

            #region CASH FLOW
            routes.MapRoute(
               name: "CashFlow",
               url: "cashflw/list",
               defaults: new { controller = "FlowMng", action = "CashFlowList" }
           );

            routes.MapRoute(
               name: "Acconts",
               url: "account/list",
               defaults: new { controller = "FlowMng", action = "AccountsList" }
           );
            #endregion

            routes.MapRoute(
                name: "Default",
                url: "{controller}/{action}/{id}",
                defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}
