﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using System.Web.Security;
using VahaInetpub.Models;
using VahaInetpub.Utility;

namespace VahaInetpub.Controllers
{
    public class LoginController : Controller
    {
        public ActionResult LoginPage()
        {
            String lang = CommonConfiguration.SiteLanguage();

            if (Request.Cookies["SunLoginCookie"] == null || Request.Cookies["SunLoginCookie"].Value == "")
            {
                Session.Abandon();
                Response.Cookies.Add(new HttpCookie("ASP.NET_SessionId", ""));
                AddRedirCookie();
                Response.Redirect(Request.Path);
            }

            try
            {
                FormsAuthenticationTicket ticket = FormsAuthentication.Decrypt(Request.Cookies["SunLoginCookie"].Value);
                if (ticket == null || ticket.Expired == true)
                    throw new Exception();
                RemoveRedirCookie();
            }
            catch
            {
                AddRedirCookie();
                Response.Redirect(Request.Path);
            }


            String rememberUserEmail = "";
            HttpCookie rememberCookie = new HttpCookie("SunUserSettings");
            rememberCookie = Request.Cookies["SunUserSettings"];

            if (rememberCookie != null && rememberCookie["UserEmail"] != null)
            {
                rememberUserEmail = (String)rememberCookie["UserEmail"];
            }

            Dictionary<string, string> PageLabels = new Dictionary<string, string>();
            PageLabels["LOGIN_PAGE_BOX_MSG"] = StringResources.getString(StringResources.RES_MSG, "LOGIN_PAGE_BOX_MSG", lang); ;
            PageLabels["LBL_USER_EMAIL"] = StringResources.getString(StringResources.RES_LABEL, "LBL_USER_EMAIL", lang); ;
            PageLabels["LBL_PWD"] = StringResources.getString(StringResources.RES_LABEL, "LBL_PWD", lang); ;
            PageLabels["LBL_BTN_LOGIN"] = StringResources.getString(StringResources.RES_LABEL, "LBL_BTN_LOGIN", lang); ;
            PageLabels["LBL_REMEMBER_ME"] = StringResources.getString(StringResources.RES_LABEL, "LBL_REMEMBER_ME", lang); ;
            PageLabels["LBL_FORGOT_PWD"] = StringResources.getString(StringResources.RES_LABEL, "LBL_FORGOT_PWD", lang); ;
            PageLabels["R_USER_EMAIL"] = rememberUserEmail;
            PageLabels["R_CHECKED"] = !rememberUserEmail.Equals("") ? "1" : "0";
            PageLabels["R_USER_EMAIL"] = rememberUserEmail;
            PageLabels["R_CHECKED"] = !rememberUserEmail.Equals("")?"1":"0";

            ViewBag.PageLabels = PageLabels;
            ViewBag.Title = StringResources.getString(StringResources.RES_PAGE_TITLE, "LoginPage", lang);


            return View();
        }

        public JsonResult DoLogin()
        {
            String lang = CommonConfiguration.SiteLanguage();
            String useremail = (String)Request["sun_login_useremail"];
            String password = (String)Request["sun_login_password"];
            String remember = Request["sun_rememberlogin"] != null ? (String)Request["sun_rememberlogin"] : "off";

            JavaScriptSerializer js = new JavaScriptSerializer();
            GenericJsonError jsonResp = new GenericJsonError(false, "", "ERR:DoLogin:100", "");

            if (useremail == null || useremail.Equals("") || password == null || password.Equals(""))
            {
                String errMsg = StringResources.getString(StringResources.RES_ERR, "LOGIN_PAGE_INV_FORM_INPUT", lang);
                jsonResp = UnexpectedErrorModal.GenerateModal(lang, errMsg, "infoModal");
                return Json(jsonResp);
            }

            UserObject userObject = new UserObject();
            userObject.Email = useremail;
            userObject.Pwd = password;

            String[] result = userObject.Login(out userObject);

            if (result != null && result[0].Equals("true") && result[1].Equals("") && userObject != null)
            {

                if (remember.Equals("on"))
                {
                    HttpCookie rememberCookie = new HttpCookie("SunUserSettings");
                    rememberCookie["Language"] = lang;
                    rememberCookie["UserEmail"] = useremail;
                    rememberCookie.Expires = DateTime.Now.AddMonths(1);
                    rememberCookie.HttpOnly = true;
                    Response.Cookies.Add(rememberCookie);
                }
                else
                {
                    HttpCookie rememberCookie = new HttpCookie("SunUserSettings", "");
                    rememberCookie.HttpOnly = true;
                    Response.Cookies.Add(rememberCookie);
                }

                string guid = Guid.NewGuid().ToString();
                
                HttpCookie AuthTokenCookie = new HttpCookie("SunAuthToken", guid);
                AuthTokenCookie.Expires = DateTime.Now.AddMinutes(40);
                AuthTokenCookie.HttpOnly = true;

                Response.Cookies.Add(AuthTokenCookie);
                Response.Cookies.Add(new HttpCookie("SunLoginCookie", ""));

                UserSessionData userInfoItem = new UserSessionData();
                userInfoItem.Name = (userObject.Name).Trim();
                userInfoItem.Surname = userObject.Surname;
                userInfoItem.MidName = userObject.MidName;
                userInfoItem.AuthToken = guid;
                userInfoItem.UserEmail = userObject.Email;
                userInfoItem.Role = userObject.Role.ToString();
                userInfoItem.ID = userObject.ID;
                Session["UserSessionData"] = userInfoItem;
                Session["MustChangePwd"] = 0;

                jsonResp = new GenericJsonError(true, "", "", "");
                return Json(jsonResp);
            }
            else
            {
                HttpCookie rememberCookie = new HttpCookie("SunUserSettings");
                rememberCookie = Request.Cookies["SunUserSettings"];

                if (rememberCookie != null && rememberCookie["UserEmail"] != null)
                {
                    String rememberUserEmail = (String)rememberCookie["UserEmail"];

                    if (!rememberUserEmail.Equals(useremail))
                    {
                        HttpCookie forgetCookie = new HttpCookie("SunUserSettings", "");
                        forgetCookie.HttpOnly = true;
                        Response.Cookies.Add(forgetCookie);
                    }
                }

                String ModalTitle = StringResources.getString(StringResources.RES_LABEL, "LBL_LOGIN_MODAL_TITLE", lang);
                String ModalBtn = StringResources.getString(StringResources.RES_LABEL, "LBL_CLOSE", lang);

                ErrorModalData errData = new ErrorModalData(
                     "infoModal", ModalTitle, "F",
                     result[2],
                     ModalBtn, "default", "dismiss",
                     "", "", ""
                 );

                Session["ModalData"] = errData;

                jsonResp = new GenericJsonError(false, "", "", "");
                return Json(jsonResp);
            }
        }

        public ActionResult RememberPwdPage()
        {
            String lang = CommonConfiguration.SiteLanguage();

            Dictionary<string, string> PageLabels = new Dictionary<string, string>();
            PageLabels["REMEMBER_PWD_PAGE_BOX_MSG"] = StringResources.getString(StringResources.RES_MSG, "REMEMBER_PWD_PAGE_BOX_MSG", lang);
            PageLabels["LBL_USER_EMAIL"] = StringResources.getString(StringResources.RES_LABEL, "LBL_USER_EMAIL", lang);
            PageLabels["LBL_BTN_SEND"] = StringResources.getString(StringResources.RES_LABEL, "LBL_BTN_SEND", lang);
            PageLabels["LBL_REDIRECT_LOGIN"] = StringResources.getString(StringResources.RES_LABEL, "LBL_REDIRECT_LOGIN", lang);

            ViewBag.PageLabels = PageLabels;
            ViewBag.Title = StringResources.getString(StringResources.RES_PAGE_TITLE, "RememberPwdPage", lang);

            return View();
        }

        public ActionResult UserMustChangePwdPage()
        {
            String lang = CommonConfiguration.SiteLanguage();
            UserSessionData dataItem = (UserSessionData)Session["UserSessionData"];

            String MustChangeConfirmMsg = String.Format(StringResources.getString(StringResources.RES_MSG, "MUST_CHANGE_PWD_PAGE_BOX_MSG", lang), dataItem.UserEmail);
            Dictionary<string, string> PageLabels = new Dictionary<string, string>();
            PageLabels["MUST_CHANGE_PWD_PAGE_BOX_MSG"] = MustChangeConfirmMsg;
            PageLabels["LBL_USER_EMAIL"] = StringResources.getString(StringResources.RES_LABEL, "LBL_USER_EMAIL", lang);
            PageLabels["LBL_OLDPWD"] = StringResources.getString(StringResources.RES_LABEL, "LBL_OLD_PWD", lang);
            PageLabels["LBL_PWD"] = StringResources.getString(StringResources.RES_LABEL, "LBL_PWD", lang);
            PageLabels["LBL_PWD_RE"] = StringResources.getString(StringResources.RES_LABEL, "LBL_PWD_RE", lang);
            PageLabels["LBL_BTN_SEND"] = StringResources.getString(StringResources.RES_LABEL, "LBL_BTN_SEND", lang);
            PageLabels["LBL_REDIRECT_LOGIN"] = StringResources.getString(StringResources.RES_LABEL, "LBL_REDIRECT_LOGIN", lang);

            ViewBag.PageLabels = PageLabels;
            ViewBag.Title = StringResources.getString(StringResources.RES_PAGE_TITLE, "UserMustChangePwdPage", lang);

            return View();
        }

        public ActionResult RedirectPage()
        {
            String lang = CommonConfiguration.SiteLanguage();

            return View();
        }

        public JsonResult ConfirmLogout()
        {
            String lang = CommonConfiguration.SiteLanguage();
            String logoutconfirmMsg = StringResources.getString(StringResources.RES_MSG, "CONFIRM_LOGOUT", lang);
            String modalTitle = StringResources.getString(StringResources.RES_LABEL, "TOP_MENU_LOGOUT", lang);
            String btnConfirm = StringResources.getString(StringResources.RES_LABEL, "LBL_YES", lang);
            String btnCancel = StringResources.getString(StringResources.RES_LABEL, "LBL_CANCEL", lang);

            ErrorModalData infoModal = new ErrorModalData(
                "infoModal", modalTitle, "I",
                logoutconfirmMsg,
                btnConfirm, "primary", "doLogout()",
                btnCancel, "default", "dismiss"
            );

            infoModal.Btn1Loading = "...";

            Session["ModalData"] = infoModal;

            JavaScriptSerializer js = new JavaScriptSerializer();
            GenericJsonError response = new GenericJsonError(true, "", "0", "");
            return Json(response, JsonRequestBehavior.AllowGet);

        }

        public JsonResult DoLogout()
        {
            JavaScriptSerializer js = new JavaScriptSerializer();
            GenericJsonError response = new GenericJsonError(true, "", "0", "");

            Session.Remove("UserSessionData");
            Session.Remove("MustChangePwd");
            PageDefinitonData.ClearBreadCrumbSessionData(Session);
            Session.Abandon();

            Response.Cookies.Add(new HttpCookie("ASP.NET_SessionId", ""));
            Response.Cookies.Add(new HttpCookie("SunAuthToken", ""));
            Response.Cookies.Add(new HttpCookie("SunLoginCookie", ""));

            return Json(response, JsonRequestBehavior.AllowGet);
        }

        [NoCache]
        public JsonResult HasValidSession()
        {
            if (Session["UserSessionData"] == null)
            {
                String lang = CommonConfiguration.SiteLanguage();
                String LblError = StringResources.getString(StringResources.RES_LABEL, "LBL_SESSION_ERROR", lang);
                String LblClose = StringResources.getString(StringResources.RES_LABEL, "LBL_OK", lang);
                String MsgError = StringResources.getString(StringResources.RES_ERR, "USER_SESSION_IVALID", lang);

                ErrorModalData infoModal = new ErrorModalData(
                    "logoutModal", LblError, "F",
                    MsgError,
                    LblClose, "default", "window.location.href='/login'",
                    "", "", ""
                );

                Session["ModalData"] = infoModal;

                GenericJsonError response = new GenericJsonError(false, "", "", "");
                return Json(response, JsonRequestBehavior.AllowGet);
            }
            else
            {
                GenericJsonError response = new GenericJsonError(true, "", "", "");
                return Json(response, JsonRequestBehavior.AllowGet);
            }
        }

        private void RemoveRedirCookie()
        {
            HttpCookie PalmarisWebLoginCookie = new HttpCookie("SunLoginCookie", "");
            PalmarisWebLoginCookie.HttpOnly = true;

            Response.Cookies.Add(PalmarisWebLoginCookie);
        }

        private void AddRedirCookie()
        {
            FormsAuthenticationTicket ticket =
            new FormsAuthenticationTicket(1, "SunLoginCookieVal", DateTime.Now, DateTime.Now.AddSeconds(15), false, "");
            string encryptedText = FormsAuthentication.Encrypt(ticket);

            HttpCookie SunCookie = new HttpCookie("SunLoginCookie", encryptedText);
            SunCookie.HttpOnly = true;
            try
            {
                Response.Cookies.Add(SunCookie);
            }
            catch
            {

            }
        }

        public JsonResult LoginWithApi()
        {
            String lang = CommonConfiguration.SiteLanguage();
            String remember = Request["sun_rememberlogin"] != null ? (String)Request["sun_rememberlogin"] : "off";

            JavaScriptSerializer js = new JavaScriptSerializer();
            GenericJsonError jsonResp = new GenericJsonError(false, "", "ERR:DoLogin:100", "");

            UserObject userObject = new UserObject();
            userObject.Pwd = "123456";
            userObject.Email = "canalioguz@gmail.com";

            String[] result = userObject.Login(out userObject);

            if (result != null && result[0].Equals("true") && result[1].Equals("") && userObject != null)
            {

                if (remember.Equals("on"))
                {
                    HttpCookie rememberCookie = new HttpCookie("SunUserSettings");
                    rememberCookie["Language"] = lang;
                    rememberCookie["UserEmail"] = "canalioguz@gmail.com";
                    rememberCookie.Expires = DateTime.Now.AddMonths(1);
                    rememberCookie.HttpOnly = true;
                    Response.Cookies.Add(rememberCookie);
                }
                else
                {
                    HttpCookie rememberCookie = new HttpCookie("SunUserSettings", "");
                    rememberCookie.HttpOnly = true;
                    Response.Cookies.Add(rememberCookie);
                }

                string guid = Guid.NewGuid().ToString();

                HttpCookie AuthTokenCookie = new HttpCookie("SunAuthToken", guid);
                AuthTokenCookie.Expires = DateTime.Now.AddMinutes(40);
                AuthTokenCookie.HttpOnly = true;

                Response.Cookies.Add(AuthTokenCookie);
                Response.Cookies.Add(new HttpCookie("SunLoginCookie", ""));

                UserSessionData userInfoItem = new UserSessionData();
                userInfoItem.Name = (userObject.Name + " " + userObject.MidName).Trim();
                userInfoItem.Surname = userObject.Surname;
                userInfoItem.AuthToken = guid;
                userInfoItem.UserEmail = userObject.Email;
                userInfoItem.Role = userObject.Role.ToString();
                userInfoItem.ID = userObject.ID;
                Session["UserSessionData"] = userInfoItem;
                Session["MustChangePwd"] = 0;

                jsonResp = new GenericJsonError(true, "", "", "");
                return Json(jsonResp);
            }
            else
            {
                HttpCookie rememberCookie = new HttpCookie("SunUserSettings");
                rememberCookie = Request.Cookies["SunUserSettings"];

                if (rememberCookie != null && rememberCookie["UserEmail"] != null)
                {
                    String rememberUserEmail = (String)rememberCookie["UserEmail"];

                    if (!rememberUserEmail.Equals("canalioguz@gmail.com"))
                    {
                        HttpCookie forgetCookie = new HttpCookie("SunUserSettings", "");
                        forgetCookie.HttpOnly = true;
                        Response.Cookies.Add(forgetCookie);
                    }
                }

                String ModalTitle = StringResources.getString(StringResources.RES_LABEL, "LBL_LOGIN_MODAL_TITLE", lang);
                String ModalBtn = StringResources.getString(StringResources.RES_LABEL, "LBL_CLOSE", lang);

                ErrorModalData errData = new ErrorModalData(
                     "infoModal", ModalTitle, "F",
                     result[2],
                     ModalBtn, "default", "dismiss",
                     "", "", ""
                 );

                Session["ModalData"] = errData;

                jsonResp = new GenericJsonError(false, "", "", "");
                return Json(jsonResp);
            }
        }
    }
}
