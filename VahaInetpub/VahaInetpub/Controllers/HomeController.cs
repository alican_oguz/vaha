﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using VahaInetpub.Utility;
using VahaInetpub.Models;
using Newtonsoft.Json;

namespace VahaInetpub.Common
{
    public class HomeController : Controller
    {
        [CustomAuthorize]
        public ActionResult Index()
        {
            String lang = CommonConfiguration.SiteLanguage();

            PageDefinitonData pData = new PageDefinitonData("Dashboard", true);
            pData.SaveBreadCrumbData(Session);

            return View();
        }

        [CustomAuthorize]
        public ActionResult CreateContract()
        {
            String lang = CommonConfiguration.SiteLanguage();

            PageDefinitonData pData = new PageDefinitonData("Dashboard", true);

            Dictionary<String, String> FormLabels = new Dictionary<String, String>();

            FormLabels.Add("UserType", StringResources.getString(StringResources.RES_LABEL, "KUllanıcı Tipi", lang));
            FormLabels.Add("Sellers", StringResources.getString(StringResources.RES_LABEL, "Satıcılar", lang));
            FormLabels.Add("PurchaseOrder", StringResources.getString(StringResources.RES_LABEL, "Alış Emri", lang));
            FormLabels.Add("TotalPrice", StringResources.getString(StringResources.RES_LABEL, "Tutar", lang));
            FormLabels.Add("Currency", StringResources.getString(StringResources.RES_LABEL, "Döviz Kuru", lang));
            FormLabels.Add("DeliveryDate", StringResources.getString(StringResources.RES_LABEL, "İşlem Tarihi", lang));
            FormLabels.Add("IncoTerm", StringResources.getString(StringResources.RES_LABEL, "Ödeme Şekli", lang));
            FormLabels.Add("Explantation", StringResources.getString(StringResources.RES_LABEL, "Açıklama", lang));         
            
            ViewBag.FormLabels = FormLabels;

            pData.SaveBreadCrumbData(Session);

            return View();
        }

        [NoCache]
        public ActionResult DoCreateContract()
        {
            GenericJsonError response = new GenericJsonError(true, "", "", "");
            String lang = CommonConfiguration.SiteLanguage();

            response = new GenericJsonError(false, "", "", "");
            return Json(response);
        }

        //[NoCache]
        //public JsonResult DoLike(string id)
        //{
        //    GenericJsonError response = new GenericJsonError(true, "", "", "");
        //    String lang = CommonConfiguration.SiteLanguage();

        //    String modalHeader = StringResources.getString(StringResources.RES_LABEL, "LBL_NEWS_ADD_NEW", lang);
        //    String modalBody = StringResources.getString(StringResources.RES_MSG, "GENERIC_OPERATION_SUCCESS", lang);
        //    String modalClose = StringResources.getString(StringResources.RES_LABEL, "LBL_OK", lang);

        //    try
        //    {
        //        NewsLikeObject newUser = new NewsLikeObject();
        //        newUser.NewsId = int.Parse(id);
        //        newUser.UserEmail = CommonConfiguration.SessionUserName();
        //        String[] opResult = newUser.Create();
        //        if (opResult != null && opResult.Length == 3 && opResult[0] != null && opResult[0].Equals("true"))
        //        {
        //            return Json(response);
        //        }
        //        else if (opResult != null && opResult.Length == 3 && opResult[0] != null && opResult[0].Equals("false"))
        //        {
        //            String errCode = opResult[1] != null ? opResult[1] : CommonConfiguration.ERR_CODE_OPERATION_FAILED;
        //            modalBody = opResult[2] != null ? String.Format(StringResources.getString(StringResources.RES_ERR, "GENERIC_OPERATION_FAILED", lang), opResult[2]) : String.Format(StringResources.getString(StringResources.RES_ERR, "GENERIC_OPERATION_FAILED_NO_ERR_DESC", lang), CommonConfiguration.ERR_CODE_OPERATION_FAILED);
        //            modalClose = StringResources.getString(StringResources.RES_LABEL, "LBL_CLOSE", lang);

        //            response = new GenericJsonError(false, "", errCode, "");
        //            return Json(response);
        //        }
        //        else
        //        {
        //            String errCode = CommonConfiguration.ERR_CODE_OPERATION_FAILED_NO_ERR_DESC;
        //            modalBody = String.Format(StringResources.getString(StringResources.RES_ERR, "GENERIC_OPERATION_FAILED_NO_ERR_DESC", lang), CommonConfiguration.ERR_CODE_OPERATION_FAILED_NO_ERR_DESC);
        //            modalClose = StringResources.getString(StringResources.RES_LABEL, "LBL_CLOSE", lang);

        //            response = new GenericJsonError(false, "", errCode, "");
        //            return Json(response);
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        String errCode = CommonConfiguration.ERR_CODE_OPERATION_EXCEPTION;
        //        modalBody = String.Format(StringResources.getString(StringResources.RES_ERR, "GENERIC_OPERATION_EXCEPTION", lang), CommonConfiguration.ERR_CODE_OPERATION_EXCEPTION);
        //        modalClose = StringResources.getString(StringResources.RES_LABEL, "LBL_CLOSE", lang);

        //        response = new GenericJsonError(false, "", errCode, ex.Message);
        //        return Json(response);
        //    }
        //}

        //[NoCache]
        //public JsonResult DoComment(string id,string comment)
        //{
        //    GenericJsonError response = new GenericJsonError(true, "", "", "");
        //    String lang = CommonConfiguration.SiteLanguage();

        //    String modalHeader = StringResources.getString(StringResources.RES_LABEL, "LBL_NEWS_ADD_NEW", lang);
        //    String modalBody = StringResources.getString(StringResources.RES_MSG, "GENERIC_OPERATION_SUCCESS", lang);
        //    String modalClose = StringResources.getString(StringResources.RES_LABEL, "LBL_OK", lang);

        //    try
        //    {
        //        NewsCommentObject newUser = new NewsCommentObject();
        //        newUser.NewsId = int.Parse(id);
        //        newUser.UserEmail = CommonConfiguration.SessionUserName();
        //        newUser.Comment = comment;
        //        String[] opResult = newUser.Create();
        //        if (opResult != null && opResult.Length == 3 && opResult[0] != null && opResult[0].Equals("true"))
        //        {
        //            return Json(response);
        //        }
        //        else if (opResult != null && opResult.Length == 3 && opResult[0] != null && opResult[0].Equals("false"))
        //        {
        //            String errCode = opResult[1] != null ? opResult[1] : CommonConfiguration.ERR_CODE_OPERATION_FAILED;
        //            modalBody = opResult[2] != null ? String.Format(StringResources.getString(StringResources.RES_ERR, "GENERIC_OPERATION_FAILED", lang), opResult[2]) : String.Format(StringResources.getString(StringResources.RES_ERR, "GENERIC_OPERATION_FAILED_NO_ERR_DESC", lang), CommonConfiguration.ERR_CODE_OPERATION_FAILED);
        //            modalClose = StringResources.getString(StringResources.RES_LABEL, "LBL_CLOSE", lang);

        //            response = new GenericJsonError(false, "", errCode, "");
        //            return Json(response);
        //        }
        //        else
        //        {
        //            String errCode = CommonConfiguration.ERR_CODE_OPERATION_FAILED_NO_ERR_DESC;
        //            modalBody = String.Format(StringResources.getString(StringResources.RES_ERR, "GENERIC_OPERATION_FAILED_NO_ERR_DESC", lang), CommonConfiguration.ERR_CODE_OPERATION_FAILED_NO_ERR_DESC);
        //            modalClose = StringResources.getString(StringResources.RES_LABEL, "LBL_CLOSE", lang);

        //            response = new GenericJsonError(false, "", errCode, "");
        //            return Json(response);
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        String errCode = CommonConfiguration.ERR_CODE_OPERATION_EXCEPTION;
        //        modalBody = String.Format(StringResources.getString(StringResources.RES_ERR, "GENERIC_OPERATION_EXCEPTION", lang), CommonConfiguration.ERR_CODE_OPERATION_EXCEPTION);
        //        modalClose = StringResources.getString(StringResources.RES_LABEL, "LBL_CLOSE", lang);

        //        response = new GenericJsonError(false, "", errCode, ex.Message);
        //        return Json(response);
        //    }
        //}

        //[NoCache]
        //public JsonResult GetLikeCommentCount(string id)
        //{
        //    GenericJsonError response = new GenericJsonError(true, "", "", "");
        //    String lang = CommonConfiguration.SiteLanguage();

        //    String modalHeader = StringResources.getString(StringResources.RES_LABEL, "LBL_NEWS_ADD_NEW", lang);
        //    String modalBody = StringResources.getString(StringResources.RES_MSG, "GENERIC_OPERATION_SUCCESS", lang);
        //    String modalClose = StringResources.getString(StringResources.RES_LABEL, "LBL_OK", lang);

        //    try
        //    {
        //        NewsObject newUser = new NewsObject();
        //        NewsObject opResult = newUser.GetLikeCommentCount(id);
        //        response.data1 = JsonConvert.SerializeObject(opResult);
        //        return Json(response);

        //    }
        //    catch (Exception ex)
        //    {
        //        String errCode = CommonConfiguration.ERR_CODE_OPERATION_EXCEPTION;
        //        modalBody = String.Format(StringResources.getString(StringResources.RES_ERR, "GENERIC_OPERATION_EXCEPTION", lang), CommonConfiguration.ERR_CODE_OPERATION_EXCEPTION);
        //        modalClose = StringResources.getString(StringResources.RES_LABEL, "LBL_CLOSE", lang);

        //        response = new GenericJsonError(false, "", errCode, ex.Message);
        //        return Json(response);
        //    }
        //}
    }
}
