﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using VahaInetpub.Models;
using VahaInetpub.Utility;

namespace VahaInetpub.Controllers
{
    public class FlowMngController : Controller
    {
        [CustomAuthorize]
        public ActionResult CashFlowList()
        {
            String lang = CommonConfiguration.SiteLanguage();
            SessionParamsUtil.ClearParameterTable();

            PageDefinitonData pData = new PageDefinitonData("Nakit Akış Listesi", false);
            pData.AddBreadCrumbItem("Nakit Akış Yönetimi", false);
            pData.SaveBreadCrumbData(Session);


            List<CashFlowObject> cashFlowList = new List<CashFlowObject>();

            cashFlowList.Add(new CashFlowObject { BuySell = "Alış", Amount = "2.312,00 TL", Date = "11.08.2017", Description = "03.08 Tarihli Satış işlemine ait ", Status = "Tamamlandı" });
            cashFlowList.Add(new CashFlowObject { BuySell = "Satış", Amount = "4.084,00 TL", Date = "30.07.2017", Description = "03.08 Tarihli transfer işleme ait ", Status = "Onay Bekliyor" });
            cashFlowList.Add(new CashFlowObject { BuySell = "Alış", Amount = "678,00 TL", Date = "12.09.2017", Description = "03.08 Tarihli işleme ait ", Status = "Tamamlandı" });
            cashFlowList.Add(new CashFlowObject { BuySell = "Alış", Amount = "10.665,00 TL", Date = "11.08.2017", Description = "03.08 Tarihli işleme ait ", Status = "Tamamlandı" });
            cashFlowList.Add(new CashFlowObject { BuySell = "Satış", Amount = "12,00 TL", Date = "11.08.2017", Description = "03.08 Tarihli işleme ait ", Status = "Tamamlandı" });
            cashFlowList.Add(new CashFlowObject { BuySell = "Alış", Amount = "2.312,00 TL", Date = "07.08.2017", Description = "03.08 Tarihli işleme ait ", Status = "Tamamlandı" });
            cashFlowList.Add(new CashFlowObject { BuySell = "Satış", Amount = "2.312,00 TL", Date = "11.08.2017", Description = "03.08 Tarihli işleme ait ", Status = "Tamamlandı" });
            cashFlowList.Add(new CashFlowObject { BuySell = "Satış", Amount = "2.312,00 TL", Date = "05.08.2017", Description = "03.08 Tarihli işleme ait ", Status = "Tamamlandı" });
            cashFlowList.Add(new CashFlowObject { BuySell = "Alış", Amount = "2.312,00 TL", Date = "21.08.2017", Description = "03.08 Tarihli işleme ait ", Status = "Tamamlandı" });

            ViewBag.cashFlowList = cashFlowList;

            List<String> TableHeaders = new List<String>();
            TableHeaders.Add(StringResources.getString(StringResources.RES_LABEL, "ALIŞ/SATIŞ", lang));
            TableHeaders.Add(StringResources.getString(StringResources.RES_LABEL, "AÇIKLAMA", lang));
            TableHeaders.Add(StringResources.getString(StringResources.RES_LABEL, "TARİH", lang));
            TableHeaders.Add(StringResources.getString(StringResources.RES_LABEL, "TUTAR", lang));
            TableHeaders.Add(StringResources.getString(StringResources.RES_LABEL, "DURUM", lang));
            ViewBag.TableHeaders = TableHeaders;

            return View();
        }

        [CustomAuthorize]
        public ActionResult AccountsList()
        {
            String lang = CommonConfiguration.SiteLanguage();
            SessionParamsUtil.ClearParameterTable();

            PageDefinitonData pData = new PageDefinitonData("Hesap Listesi", false);
            pData.AddBreadCrumbItem("Global Hesap Yönetimi", false);
            pData.SaveBreadCrumbData(Session);

            List<AccountObject> accountList = new List<AccountObject>();

            accountList.Add(new AccountObject { Bank = "XYZ Bank", Balance = "2.312,00 TL", Branch = "Ankara Ticari", Number = "1234567"});
            accountList.Add(new AccountObject { Bank = "ABC Bank", Balance = "4.312,00 TL", Branch = "İstanbul Ticari", Number = "45678"});
            accountList.Add(new AccountObject { Bank = "İşbankası", Balance = "32.312,00 TL", Branch = "Levent Ticari", Number = "987654"});
            accountList.Add(new AccountObject { Bank = "İşbankası", Balance = "1.312,00 TL", Branch = "4. Levent", Number = "56789"});
            accountList.Add(new AccountObject { Bank = "ASD Bank", Balance = "512,00 TL", Branch = "Ankara Ticari", Number = "97533"});
            accountList.Add(new AccountObject { Bank = "SDF Bank", Balance = "2.432,00 TL", Branch = "Beşiktaş", Number = "4272555"});
            accountList.Add(new AccountObject { Bank = "İşbankası", Balance = "22,00 TL", Branch = "Galatasaray", Number = "73536366"});

            ViewBag.accountList = accountList;

            List<String> TableHeaders = new List<String>();
            TableHeaders.Add(StringResources.getString(StringResources.RES_LABEL, "BANKA", lang));
            TableHeaders.Add(StringResources.getString(StringResources.RES_LABEL, "ŞUBE", lang));
            TableHeaders.Add(StringResources.getString(StringResources.RES_LABEL, "HESAP NUMARASI", lang));
            TableHeaders.Add(StringResources.getString(StringResources.RES_LABEL, "BAKİYE", lang));
            ViewBag.TableHeaders = TableHeaders;

            return View();
        }

        [CustomAuthorize]
        public ActionResult ContractList()
        {
            String lang = CommonConfiguration.SiteLanguage();
            SessionParamsUtil.ClearParameterTable();

            PageDefinitonData pData = new PageDefinitonData("Kontrat Listesi", false);
            pData.AddBreadCrumbItem("Kontrat Yönetimi", false);
            pData.SaveBreadCrumbData(Session);
            UserSessionData user = (UserSessionData)Session["UserSessionData"];

            UserObjectSet userObjectSet = new UserObjectSet();
            userObjectSet.Load();
            List<UserObject> userList = userObjectSet.UserObjectList;

            ContractObjects contractObjectSet = new ContractObjects();
            List<ContractObjects> buyerContracts = contractObjectSet.GetContractsListByBuyerID(user.ID);
            List<ContractObjects> sellerContracts = contractObjectSet.GetContractsListBySellerID(user.ID);
            buyerContracts.AddRange(sellerContracts);

            foreach(ContractObjects con in buyerContracts)
            {
                con.ContractSellerName = userList.Where(uer => uer.ID == con.ContractSellerId).FirstOrDefault().Name + " " + userList.Where(uer => uer.ID == con.ContractSellerId).FirstOrDefault().Surname;
                con.ContractBuyerName = userList.Where(uer => uer.ID == con.ContractBuyerId).FirstOrDefault().Name + " " + userList.Where(uer => uer.ID == con.ContractBuyerId).FirstOrDefault().Surname;
            }

            ViewBag.contractList = buyerContracts;

            List<String> TableHeaders = new List<String>();
            TableHeaders.Add(StringResources.getString(StringResources.RES_LABEL, "KONTRAT NUMARASI", lang));
            TableHeaders.Add(StringResources.getString(StringResources.RES_LABEL, "SATICI ADI", lang));
            TableHeaders.Add(StringResources.getString(StringResources.RES_LABEL, "ALICI ADI", lang));
            TableHeaders.Add(StringResources.getString(StringResources.RES_LABEL, "TUTAR", lang));
            TableHeaders.Add(StringResources.getString(StringResources.RES_LABEL, "TARİH", lang));
            TableHeaders.Add(StringResources.getString(StringResources.RES_LABEL, "FATURA NUMARASI", lang));
            TableHeaders.Add(StringResources.getString(StringResources.RES_LABEL, "DURUM", lang));
            ViewBag.TableHeaders = TableHeaders;

            ViewBag.AddNew = StringResources.getString(StringResources.RES_LABEL, "Yeni Kontrat Oluştur", lang);

            return View();
        }

    }
}
